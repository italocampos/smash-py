from pade.misc.utility import start_loop
from smash import tools, networks, config
import color

# Getting the net model defined at smash.config
if config.NETWORK_MODEL == '10-bus':
	net = networks.network10bus()
elif config.NETWORK_MODEL == '16-bus':
	net = networks.network16bus()
elif config.NETWORK_MODEL == '33-bus':
	net = networks.network33bus()
elif config.NETWORK_MODEL == '119-bus':
	net = networks.network119bus()
else:
	raise TypeError('No such data for the test system')

print(color.yellow('[SMASH-PY] --> ') + 'Starting Smash-py with %s.' % color.cyan(config.NETWORK_MODEL, 'b'))

if __name__ == '__main__':
	start_loop(tools.build_system(net))