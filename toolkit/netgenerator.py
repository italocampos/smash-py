''' 
Net Generator Module
--------------------
This module meants to generate the Python commands for the network models
creation for the use with Smash-py.

Attention: The commands genarated are general, then the resulting script needs
to be reviewed before used (like creating ext buses, opened switches, bus0).
Check it out!!!

@author : @italocampos
'''

import random, pandas, networks
from pathlib import Path


# Put the home path of the system here
HOME_SIMULATIONS = str(Path.home()) + '/Documents/UFPA/LAAI/Simulations/'



def write_file(content, file_name = './output.py'):
	''' Writes the provided str in a Python file.

	Paramenters
	-----------
	content : str
		The str to be written in the output file.
	file_name : str, optional
		The name of the output file. You can also pass a file path as a file
		name. The default output path is './output.py'.
	'''

	try:
		file = open(file_name, 'r')
		lines = file.readlines()
		lines.append(content + '\n')
		file = open(file_name, 'w')
		file.writelines(lines)
		file.close()
	except FileNotFoundError:
		file = open(file_name, 'w')
		file.write(content + '\n')
		file.close()


def create_net(csv_file):
	''' Returns the commands of creation for a pandapower network.

	Parameters
	----------
	csv_file : str
		The path to the csv file containing the data of the network to be
		created. The columns of the table must be named respectively:
		ID, FROM_BUS, TO_BUS, R_OHM, X_OHM, C_NF, MAX_I_KA, TO_BUS_MW,
		TO_BUS_MVAR.
	
	Returns
	-------
	str
		The str of the Python command for pandapower network creation.
	'''

	command = str()
	matrix = pandas.read_csv(csv_file)

	# Commands for creation of the buses
	buses = list()
	for to_bus in matrix['TO_BUS']:
		if not to_bus in buses:
			buses.append(to_bus)
			command += "bus{n} = pp.create_bus(net, name = 'bus{n}', vn_kv = 11, type = 'b')\n".format(
				n = to_bus
			)
	command += '\n'
		
	# Commands for creation of the loads
	buses = list()
	for to_bus, to_bus_mw, to_bus_mvar in zip(matrix['TO_BUS'], matrix['TO_BUS_MW'], matrix['TO_BUS_MVAR']):
		if not to_bus in buses:
			buses.append(to_bus)
			command += "load{n} = pp.create_load(net, bus{n}, p_mw = {p_mw}, q_mvar = {q_mvar}, name = 'load{n}')\n".format(
				n = to_bus,
				p_mw = to_bus_mw,
				q_mvar = to_bus_mvar,
			)
	command += '\n'

	# Commands for creation of the lines
	for id_of_line, from_bus, to_bus, r_ohm, x_ohm, c_nf, max_i in zip(matrix['ID'], matrix['FROM_BUS'], matrix['TO_BUS'], matrix['R_OHM'], matrix['X_OHM'], matrix['C_NF'], matrix['MAX_I_KA']):
		command += "line{n} = pp.create_line_from_parameters(net, bus{from_bus}, bus{to_bus}, length_km = 1.0, r_ohm_per_km = {r_ohm}, x_ohm_per_km = {x_ohm}, c_nf_per_km = {c_nf}, max_i_ka = {max_i}, name = 'line{n}')\n".format(
			n = id_of_line,
			from_bus = from_bus,
			to_bus = to_bus,
			r_ohm = r_ohm,
			x_ohm = x_ohm,
			c_nf = c_nf,
			max_i = max_i,
		)
	command += '\n'

	# Commands for creation of the switches
	for id_of_line, to_bus in zip(matrix['ID'], matrix['TO_BUS']):
		command += "sw{n} = pp.create_switch(net, bus{to_bus}, line{n}, et = 'l', type = 'LBS', closed = True, name = 'sw{n}')\n".format(
			n = id_of_line,
			to_bus = to_bus,
		)
	
	return command


def classify_lines(net, class_range = 0.3):
	''' Classificate the lines of 'net' as a standart line model of pandapower.

	Classification of the lines of the 'net', based on the proximity of the
	r_ohm value with the correspondent value in the standard types available on
	the pandapower library. The decision is made with an arbitrary criteria,
	using a range to match the lines with the models. A line can match more
	than one model, but this function will return the model that match first,
	considering a ascending ordering of the r_ohm parameter of the models.

	Parameters
	----------
	net : pandapower.auxiliary.pandapowerNet
		The pandapower net to classificate the lines.
	class_range : float, optional, default = 0.3
		The range of the classification of the models.

	Returns
	-------
	list
		A list with the models that match with the lines of the provided 'net'.
		The ordering of the list is the same ordering of the lines in 'net'.
	'''

	pp_lib = [
		["679-AL1/86-ST1A 110.0",0.0420,0.360,9.95,1.150,"ol",679,0.00403],
		["490-AL1/64-ST1A 110.0",0.0590,0.370,9.75,0.960,"ol",490,0.00403],
		["305-AL1/39-ST1A 110.0",0.0949,0.380,9.20,0.740,"ol",305,0.00403],
		["243-AL1/39-ST1A 110.0",0.1188,0.390,9.00,0.645,"ol",243,0.00403],
		["184-AL1/30-ST1A 110.0",0.1571,0.400,8.80,0.535,"ol",184,0.00403],
		["149-AL1/24-ST1A 110.0",0.1940,0.410,8.75,0.470,"ol",149,0.00403],
		["122-AL1/20-ST1A 110.0",0.2376,0.430,8.50,0.410,"ol",122,0.00403],
		["94-AL1/15-ST1A 110.0",0.3060,0.440,8.65,0.350,"ol",94,0.00403],
		["70-AL1/11-ST1A 110.0",0.4132,0.450,8.40,0.290,"ol",70,0.00403],
		["48-AL1/8-ST1A 110.0",0.5939,0.460,8.00,0.210,"ol",48,0.00403],
		#["34-AL1/6-ST1A 20.0",0.8342,0.382,9.15,0.170,"ol",34,0.00403],
		#["24-AL1/4-ST1A 0.4",1.2012,0.335,11.25,0.140,"ol",24,0.00403],
		#["15-AL1/3-ST1A 0.4",1.8769,0.350,11.00,0.105,"ol",16,0.00403],
	]

	output = list()

	for r_ohm in net.line['r_ohm_per_km']:
		match = False
		for model in pp_lib:
			if model[1]*(1 - class_range) <= r_ohm <= model[1]*(1 + class_range):
				output.append(model)
				match = True
				break
		if not match:
			output.append(["UNKNOWN CLASS", 0, 0, 0, 0, "", 0, 0])
	
	return output

 

# Generating the creation commands for 33-bus 
write_file(create_net('data/networks/33-bus_classified.csv'), '33-bus_creation-commands.py')
write_file(create_net('data/networks/119-bus_classified.csv'), '119-bus_creation-commands.py')

# Classificating the lines of network
'''
#classes = classify_lines(networks.network119bus())
#classes = classify_lines(networks.network33bus())
# > Creating a csv file to the returned classes
output = "STANDARD_TYPE,R_OHM,X_OHM,C_NF_PER_KM,MAX_I_KA,TYPE,Q_MM2,ALPHA\n"
for line in classes:
	for cell in line:
		output += str(cell) + ','
	output += '\n'
write_file(output, 'data/networks/119-bus_classes.csv')
'''