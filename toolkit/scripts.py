'''
Analyzing module
----------------

This module validates and analyzes some parameters of the results of Smash-py.

@author: @italocampos
'''

import evaluation as ev, color, util


default_topologies = {
    '10-bus': [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0],
    '16-bus': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
    '33-bus': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
    '119-bus': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
}

def evaluate_solutions(solutions, faults):
    ''' Evaluates the solutions returned by TS

    The lists 'solutions' and 'faults' must have the same length. See the
    parameter description to more details.
    
	Parameters
	----------
	solutions : list
		A list with the solutions to be analyzed.
    faults : list
        A list of lists that contains the faulted lines for each solutions.
	
	Raises
	------
	Exception
		In case the solution don't match any supported network model
	'''

    for i, sol in enumerate(solutions):
        print(color.blue('\nSIMULATION #%d ------------' % (i + 1)))
        validation = ev.validate(sol)
        if validation:
            if len(sol) == len(default_topologies['10-bus']):
                default = default_topologies['10-bus'].copy()
            elif len(sol) == len(default_topologies['16-bus']):
                default = default_topologies['16-bus'].copy()
            elif len(sol) == len(default_topologies['33-bus']):
                default = default_topologies['33-bus'].copy()
            elif len(sol) == len(default_topologies['119-bus']):
                default = default_topologies['119-bus'].copy()
            else:
                raise(Exception('No such model to this solution.'))
            
            faulted = default.copy()
            for f in faults[i]:
                faulted[f] = 0

            value = ev.value(sol)

            zeros = list()
            for j, element in enumerate(default):
                if element == 0:
                    zeros.append(j)
            
            ones = list()
            for j, element in enumerate(default):
                if element == 1:
                    ones.append(j)
            
            opened_switches = list()
            for j, element in enumerate(sol):
                if element == 0 and j not in zeros:
                    opened_switches.append(j)
            
            closed_switches = list()
            for j, element in enumerate(sol):
                if element == 1 and j not in ones:
                    closed_switches.append(j)
            
            print(color.green('VALIDATION: %s' % validation, 'b'))
            print('VALUE:', value)
            print('NAB:', len(ev.unsupplied_buses(faulted)))
            print('NRB:', value - (ev.value(default) - len(ev.unsupplied_buses(faulted))))
            print('OPENED SW:', opened_switches)
            print('CLOSED SW:', closed_switches)
        else:
            print(color.red('VALIDATION: %s' % validation, 'b'))