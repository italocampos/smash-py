''' 
Evaluation Module
-----------------
This module agregates functions to evaluate solutions in the 'vector of lines'
form ([1, 1, 1, 0, 0,...]). Use the functions to automatize the analysis
process.

@author : @italocampos
'''

from smash import networks, tools
import pandapower as pp


def _select_network(solution):
    ''' Returns the suitable Pandapower network based on the provided
    'solution'.
    
    The imput list must be the same lenght of a predefinided net model. The
    lenghts are described below:
        - len(solution) = 11: 10-bus model;
        - len(solution) = 16: 16-bus model;
        - len(solution) = 37: 33-bus model;
        - len(solution) = 132: 119-bus model.
    If a solution has a different length, this function raises an Exception.

    Parameters
    ----------
    solution : list
        The vector of binaries representing the net configuration.
    
    Returns
    -------
    pandapower.auxiliary.pandapowerNet
        The corresponted pandapower net to the provided solution.
    
    Raises
    ------
    Exception
        When a solution dont is supported by this analysis.
    '''

    if len(solution) == 11:
        net = networks.network10bus()
    elif len(solution) == 16:
        net = networks.network16bus()
    elif len(solution) == 37:
        net = networks.network33bus()
    elif len(solution) == 132:
        net = networks.network119bus()
    else:
        raise Exception('Solution not supported.')

    # Making the switching operations in the network
    for i in range(len(solution)):
        net.switch.closed[i] = solution[i]


    return net


def validate(solution):
    ''' Checks is a solution is valid or not.
    
    The solution is a vector of binaries that indicates the final configuration
    of switches in the network model. The imput vector must be the same lenght
    of a predefinided net model. The lenghts are described below:
        - len(solution) = 11: 10-bus model;
        - len(solution) = 16: 16-bus model;
        - len(solution) = 37: 33-bus model;
        - len(solution) = 132: 119-bus model.
    The validation of the model matches three network constraints, which two of
    them are defined in the smash.tools module. The three net constraints used
    in the validation are:
        a) Network must be radial;
        b) Network must respect the voltage limits;
        c) Network must respect the current limits.
    
    Parameters
    ----------
    solution : list
        The vector of binaries representing the net configuration.
    
    Returns
    -------
    bool
        True if the solutions is valid; False otherwise.
    '''
    
    # Gets the suitable net to validate
    net = _select_network(solution)

    return tools.validate_network(net)


def unsupplied_buses(solution):
    ''' Returns the list of the unsupplied buses in the 'solution'.
    
    The solution is a vector of binaries that indicates the final configuration
    of switches in the network model. The imput vector must be the same lenght
    of a predefinided net model. The lenghts are described below:
        - len(solution) = 11: 10-bus model;
        - len(solution) = 16: 16-bus model;
        - len(solution) = 37: 33-bus model;
        - len(solution) = 132: 119-bus model.
    
    Parameters
    ----------
    solution : list
        The vector of binaries representing the net configuration.
    
    Returns
    -------
    list
        The list with the id of the unsupplied buses.
    '''

    # Gets the suitable net to evaluate
    net = _select_network(solution)

    return list(pp.topology.unsupplied_buses(net))


def value(solution):
    ''' Returns the value of the 'solution' in the adopted objective function.

    The objective function adopted in this research is:
    Max(x) = x1 + x2 + ... + xi,
    Where xi are the buses recovered by the MAS.
    
    The solution is a vector of binaries that indicates the final configuration
    of switches in the network model. The imput vector must be the same lenght
    of a predefinided net model. The lenghts are described below:
        - len(solution) = 11: 10-bus model;
        - len(solution) = 16: 16-bus model;
        - len(solution) = 37: 33-bus model;
        - len(solution) = 132: 119-bus model.
    
    Parameters
    ----------
    solution : list
        The vector of binaries representing the net configuration.
    
    Returns
    -------
    int
        The value of the 'solution' in the objective function.
    '''

    # Gets the suitable net to evaluate
    net = _select_network(solution)

    return len(net.bus.values) - len(unsupplied_buses(solution))


def power_losses(solution):
    ''' Returns the total power losses (in MW) for the 'solution'.
    
    The solution is a vector of binaries that indicates the final configuration
    of switches in the network model. The imput vector must be the same lenght
    of a predefinided net model. The lenghts are described below:
        - len(solution) = 11: 10-bus model;
        - len(solution) = 16: 16-bus model;
        - len(solution) = 37: 33-bus model;
        - len(solution) = 132: 119-bus model.
    
    Parameters
    ----------
    solution : list
        The vector of binaries representing the net configuration.
    
    Returns
    -------
    numpy.float64
        The value of the total power losses (in MW) in the system described by
        the provided 'solution'.
    '''

    # Gets the suitable net to evaluate
    net = _select_network(solution)

    pp.runpp(net)
    acc = 0
    for loss in net.res_line.pl_mw:
        acc += loss

    return acc


def voltages(solution):
    ''' Returns a list with the voltages for the net described in 'solution'.

    The values of the voltages are in p.u. unity.
    
    The solution is a vector of binaries that indicates the final configuration
    of switches in the network model. The imput vector must be the same lenght
    of a predefinided net model. The lenghts are described below:
        - len(solution) = 11: 10-bus model;
        - len(solution) = 16: 16-bus model;
        - len(solution) = 37: 33-bus model;
        - len(solution) = 132: 119-bus model.
    
    Parameters
    ----------
    solution : list
        The vector of binaries representing the net configuration.
    
    Returns
    -------
    list
        The list with the voltages (vm pu) in the buses for the net described
        in 'solution'.
    '''

    # Gets the suitable net to evaluate
    net = _select_network(solution)

    pp.runpp(net)
    return list(net.res_bus.vm_pu.values)


def currents(solution):
    ''' Returns a list with the current values for the net described in
    'solution'.

    The values of the currents are in percentage.
    
    The solution is a vector of binaries that indicates the final configuration
    of switches in the network model. The imput vector must be the same lenght
    of a predefinided net model. The lenghts are described below:
        - len(solution) = 11: 10-bus model;
        - len(solution) = 16: 16-bus model;
        - len(solution) = 37: 33-bus model;
        - len(solution) = 132: 119-bus model.
    
    Parameters
    ----------
    solution : list
        The vector of binaries representing the net configuration.
    
    Returns
    -------
    list
        The list with the current values in the lines for the net described in 
        'solution'.
    '''

    # Gets the suitable net to evaluate
    net = _select_network(solution)

    pp.runpp(net)
    return list(net.res_line.loading_percent.values)