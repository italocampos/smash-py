import pandapower as pp, networkx, math, color

from smash.agents import BusAgent, NetworkAgent, LineAgent
from smash.data import DataResource
from smash import config

# Defining the network constraints
constraints = [
	{'model': '10-bus', 'V': 0.05, 'I': 0.0},
	{'model': '16-bus', 'V': 0.05, 'I': 0.0},
	{'model': '33-bus', 'V': 0.18, 'I': 0.0},
	{'model': '119-bus', 'V': 0.15, 'I': 0.0},
	]
# Getting the suitable network model set in smash.config
model =  None
for item in constraints:
	if item['model'] == config.NETWORK_MODEL:
		model = item
if model != None:
	# Defining the limit of voltage variation
	V_DELTA_RATIO = model['V']
	# Defining the limit for max current in the lines
	I_DELTA_RATIO = model['I']
else:
	raise TypeError('No such data for the test system')


def system_message(message):
	''' Print RED colored messages. Used to system messages

	Parameters
	----------
	message : str
		The message to be printed in the screen.
	'''

	print(color.yellow('[SYSTEM] --> ') + color.red(message))


def validate_network(network):
	''' Validates a pandapower network based on	adopted research criterias.
	
	Parameters
	----------
	network : pandapower.auxiliary.pandapowerNet
		The pandapower network to be validated.
	
	Returns
	-------
	bool
		The result of validation. True is the net is valid; False otherwise.
	'''

	# Criteria 1: looking for cycles
	try:
		cycle = networkx.find_cycle(pp.topology.create_nxgraph(network))
		system_message('The configuration has cycles: %s.' % cycle)
		return False
	except(networkx.exception.NetworkXNoCycle):
		pass
	
	# Criteria 2: checking the voltage limits
	try:
		pp.runpp(network)
		for voltage in network.res_bus.vm_pu:
			if not math.isnan(voltage) and not (1 - V_DELTA_RATIO) <= voltage <= (1 + V_DELTA_RATIO):
				system_message('The configuration is out of voltage limits.')
				return False
	except(pp.powerflow.LoadflowNotConverged):
		system_message('The power flow not converged.')
		return False
	
	# Criteria 3: checking the current limits
	for current in network.res_line.loading_percent:
		if not math.isnan(current) and not current <= 100 * (1 + I_DELTA_RATIO):
			system_message('The configuration is out of current limits.')
			return False
	
	return True



def build_system(net):
	''' This function builds the agents for Smash-py using the
	pandapower network passed on argument.
	'''

	# That is the agent lists
	buses = list()
	lines = list()

	# Validading the passed network
	if not validate_network(net):
		raise(Exception("The provided network violates some of the system's constraints."))

	# Creating DataResource object
	data = DataResource(net)

	# Making the agents for buses
	for index, name in enumerate(net.bus['name']):
		buses.append(BusAgent(aid = name, network_id = index, network_data = data))

	# Making the agents for lines and connecting the bus agents
	for index, name, from_bus, to_bus in zip(range(len(net.line.values)), net.line['name'], net.line['from_bus'], net.line['to_bus']):
		lines.append(LineAgent(aid = name, network_id = index, network_data = data))
		lines[-1].attach_buses([buses[from_bus].aid, buses[to_bus].aid])
		buses[from_bus].connect_neighbor({
			'bus_aid': buses[to_bus].aid,
			'bus_id': to_bus,
			'line_aid': lines[-1].aid,
			'line_id': index
		})
		buses[to_bus].connect_neighbor({
			'bus_aid': buses[from_bus].aid,
			'bus_id': to_bus,
			'line_aid': lines[-1].aid,
			'line_id': index
		})

	# Creating a bus list with all states set in True
	states = [True for _ in net.bus.values]
	# Looking for unsupplied buses and marking them as False
	for bus_index in list(list(pp.topology.unsupplied_buses(net))):
		states[bus_index] = False
	# Passing the data to DataResource
	data.set_buses_state(states)

	# Making the NetworkAgent and passing to it the network parameters
	network = NetworkAgent(config.NETWORK, net, network_data = data)
	
	return buses + lines + [network]