from pade.acl.aid import AID
from pade.core.agent import Agent
from pade.misc.utility import display
from pade.misc.thread import SharedResource

import smash.config
import smash.behaviours.bus as bus
import smash.behaviours.line as line
import smash.behaviours.network as net

import threading, color


class BusAgent(Agent):
	''' This class models a BusAgent, which is the agent that deals
	with the loads of the grid.
	
	Properties
	----------
	_neighbors : list
		A list of dict with the neighbors of this agents. The internal dicts
		must have the form:
		{'bus_aid': AID <neighbor AID>,
		'bus_id': int <neighbor network ID>,
		'line_aid': AID <line AID>
		'line_id': int <line network ID>}
	_id : int
		The id of the bus in the pandapower network.
	network_data : smash.data.DataResource
		The data structure shared between the system which simulates the
		various sensors of the system. For the buses, represents the energy
		sensors. This is a system property.
	current_proposal : pade.misc.thread.SharedResource
		The local that saves the recovery proposal that has been analyzed
		currently by this bus. This property implements thread-safe operations.
		This is a system property.
		'''

	def __init__(self, aid, network_id, network_data):
		'''
		Parameters
		----------
		aid : pade.acl.aid.AID
			The AID of this agent.
		network_id : int
			The id of this bus in the pandapower network.
		network_data : smash.data.DataResource
			The data structure shared between the system which simulates the
			various sensors of the system. For the buses, represents the energy
			sensors. This is a system property.
		'''
		
		super().__init__(aid)
		# Agent attributes
		self._neighbors = list()
		self._id = network_id

		# System attributes
		self.NETWORK = AID(smash.config.NETWORK)
		self.network_data = network_data
		self.current_proposal = SharedResource(None)


	def setup(self):
		# Energy monitoring of the bus
		self.add_behaviour(bus.EnergySensor(self, 0.5))

		# Listener to neighborhood check
		self.add_behaviour(bus.CheckNeighborhoodListener(self))

		# Listener to degree check
		self.add_behaviour(bus.DegreeRequestListener(self))

		# Listener to recovering proposals
		self.add_behaviour(bus.RecoveryListener(self))


	def num_neighbors(self):
		''' Returns the number of neighbors connected on this
		BusAgent. '''
		
		return len(self._neighbors)


	def connect_neighbor(self, neighbor):
		''' Connects a neighbor (another BusAgent) to this BusAgent
		through a dictionary. The expected parameter is a dict under
		this form:
		{'bus_aid': <neighbor AID>,
		'bus_id': <neighbor network ID>,
		'line_aid': <line AID>
		'line_id': <line network ID>} '''

		self._neighbors.append(neighbor)


	def connect_neighbors(self, neighbors):
		''' Connect a list of neighbors (others BusAgent) to this 
		BusAgent through a list of dictionaries (see the method
		self.connect_neighbor()) '''
		
		for neighbor in neighbors:
			self.connect_neighbor(neighbor)


	def get_neighbor(self, aid):
		''' Retuns a neighbor (dictionary) searching by BusAgent aid.
		'''

		for neighbor in self._neighbors:
			if aid == neighbor['bus_aid']:
				return neighbor


	@property
	def neighbors(self):
		''' Retuns the entire list of connected neighbors. '''

		return self._neighbors


	def disconnect_neighbor(self, aid):
		''' Removes a neighbor for the neighbors list. Searches by 
		aid. '''
		
		neighbor = self.get_neighbor(aid)
		if neighbor != None:
			self._neighbors.remove(neighbor)


	def disconnect_all_neighbors(self):
		''' Removes all neighbors for the neighbors list. '''

		self._neighbors.clear()


	def has_energy_supply(self):
		''' Returns True if the load has energy supply. Returns False
		otherwise. '''
		return self.network_data.get_bus_state(self.id)


	@property
	def id(self):
		return self._id
	

	@id.setter
	def id(self, value):
		self._id = value


	def __str__(self):
		neighbors = ''
		for n in self._neighbors:
			neighbors += '- {b} | {l}\n'.format(
				b = n['bus_aid'].getName(),
				l = n['line_aid'].getName())

		content = '''
BUS **********************************************
AID: {aid}
Network ID: {id}
Energy supply: {energy_supply}
--------------------------------------------------
Negihbors:
{neighbors}
**************************************************\n'''.format(
		aid = self.aid.getName(),
		id = self.id,
		energy_supply = 'OK' if self.has_energy_supply() else 'INTERRUPTED',
		neighbors = neighbors[:-1]
		)

		return content



class LineAgent(Agent):
	''' This class models a LineAgent, which is the agent that deals
	with the activities of lines and switchs of the grid.

	Properties
	----------
	_buses : list
		The list with the AID of the BusAgent connected to this LineAgent
	_id : int
		The id of this line in the pandapower network
	'''

	def __init__(self, aid, network_id, network_data):
		'''
		Parameters
		----------
		aid : str
			The local name to be used by this agent
		network_id : int
			The id of this line in the pandapower network
		network_data : smash.data.DataResource
			The DataResource object referring the physical line states
		'''

		super().__init__(aid)
		# Agent properties
		self._buses = list()
		self._id = network_id

		# System parameters
		self.network_data = network_data
		self.NETWORK = AID(smash.config.NETWORK)


	def setup(self):
		# Monitoring of the health of the line
		self.add_behaviour(line.HealthSensor(self, 0.5))

		# Listener to neighborhood check
		self.add_behaviour(line.LineHealthListener(self))

		# Listener to switch operations
		self.add_behaviour(line.SwitchOperationListener(self))


	def attach_bus(self, aid):
		''' Adds the AID of a BusAgent in the local self.loads list.
		'''
		if len(self._buses) < 2 and aid not in self._buses:
			self._buses.append(aid)
		else:
			display(self, '### The bus %s could not be attached at this line.' % aid.getName())


	def attach_buses(self, aid_list):
		''' Adds the AID of BusAgent objects passed in the input
		list. '''

		for aid in aid_list:
			self._buses.append(aid)


	@property
	def buses(self):
		''' Returns the entire self._buses list. '''

		return self._buses


	def disconnect_bus(self, aid):
		''' Removes a bus AID from the self._buses list. '''

		self._buses.remove(aid)


	def disconnect_all_buses(self):
		''' Removes all load info from the self.loads list. '''

		self._buses.clear()


	def get_line_health(self):
		''' Returns True if the line is working well. Returns False
		if the line fails. '''

		return self.network_data.get_line_state(self.id)
	

	@property
	def id(self):
		return self._id
	

	@id.setter
	def id(self, value):
		self._id = value


	def num_buses(self):
		''' Returns the number of loads attached to this LineAgent.
		'''

		return len(self._buses)


	def __str__(self):
		buses = ''
		for l in self._buses:
			buses += '- {}\n'.format(l.getName())

		content = '''
LINE *********************************************
AID: {aid}
Network ID: {id}
Line state: {health}
--------------------------------------------------
Buses:
{loads}
**************************************************\n'''.format(
		aid = self.aid.getName(),
		id = self.id,
		health = 'IN OPERATION' if self.get_line_health() else 'FAILURE',
		loads = buses[:-1])
		return content



class NetworkAgent(Agent):
	''' This class models the NetworkAgent. This agent runs the
	the powerflow calculation, check for cycles in the network, 
	simulate the energy and more.

	Attributes
	----------
	_network : pandapower.auxiliary.pandapowerNet
		The pandapower network object that models the network test model.
	_virtual_network : pandapower.auxiliary.pandapowerNet
		The virtual layer for the network test model.
	_network_lock : threading.Lock
		A Lock object to handle the read & write operations in the network
		object.
	_virtual_network_lock : threading.Lock
		A Lock object to handle the read & write operations in the virtual
		network object.
	network_data : smash.data.DataResource
		The DataResource object that is used to control the health of the
		lines and the energy status of the buses.
	recovery_state_of_the_buses : list
		This list of int indicates the current recovery state of the system.
		When no	BusAgent is recovering, all the items of the list are 0, and
		the recovery state of the system is False. The recovery state of the
		system is True otherwise.
	time : datetime
		The time when the recovery process started.
	'''

	def __init__(self, aid, network, network_data):
		'''
		Parameters
		----------
		aid : str
			The local name of this agent.
		network : pandapower.auxiliary.pandapowerNet
			The pandapower network object that models the network test model.
		network_data : smash.data.DataResource
			The DataResource object that is used to control the health of the
			lines and the energy status of the buses.
		'''

		super().__init__(aid)
		self._network = network
		self._virtual_network = network.deepcopy()
		self._network_lock = threading.Lock()
		self._virtual_network_lock = threading.Lock()
		self.network_data = network_data
		self.recovery_state_of_the_buses = [0 for _ in network.bus.values]
		self._time = None
		self._pf_locker = threading.Event()


	def setup(self):
		# Manager to switch operations
		self.add_behaviour(net.LineManager(self))

		# Line file reader
		self.add_behaviour(net.LineFileMonitor(self, 0.5))

		# Power flow listener
		self.add_behaviour(net.PowerFlowListener(self))

		# Bus data listener
		self.add_behaviour(net.BusDataListener(self))

		# Bus recovery state listener
		self.add_behaviour(net.RecoveryStateListener(self))

		# Recovery's time listener (DEBUG)
		self.add_behaviour(net.TimeReceiver(self))

		# Power Flow unlocker listener
		self.add_behaviour(net.TopologyUsageListener(self))
		self.unlock_pf()


	@property
	def network(self):
		return self._network
	

	@network.setter
	def network(self, network):
		self._network = network
	

	@property
	def virtual_network(self):
		return self._virtual_network
	

	@virtual_network.setter
	def virtual_network(self, virtual_network):
		self._virtual_network = virtual_network
	

	@property
	def network_lock(self):
		return self._network_lock
	

	@property
	def virtual_network_lock(self):
		return self._virtual_network_lock
	

	@property
	def time(self):
		return self._time
	

	@time.setter
	def time(self, time_):
		self._time = time_


	def open_switch(self, line_id):
		''' Sets the 'closed' switch attribute to 'False', openning	the switch.
		This operation is performed over the real network.
		
		Parameters
		----------
		line_id : int
			The id (in network model) of the line to be open.
		'''

		# When an operation is made over the real network, it must reflect over
		# the virtual network. The contrary is not True. The same happens in
		# the `close_switch` method.
		self.network.switch['closed'][line_id] = False
		self.open_virtual_switch(line_id)
		# DEBUG
		#display(self, color.magenta('The real switch %d was opened.' % line_id, 'd'))
	

	def open_virtual_switch(self, line_id):
		''' Sets the 'closed' switch attribute to 'False', openning	the switch.
		This operation is performed over the virtual network.
		
		Parameters
		----------
		line_id : int
			The id (in network model) of the line to be open.
		'''

		self.virtual_network.switch['closed'][line_id] = False
		# DEBUG
		#display(self, color.magenta('The virtual switch %d was opened.' % line_id, 'd'))


	def close_switch(self, line_id):
		''' Sets the 'closed' switch attribute to 'True', closing the switch.
		This operation is performed over the real network.
		
		Parameters
		----------
		line_id : int
			The id (in network model) of the line to be closed.
		'''

		self.network.switch['closed'][line_id] = True
		self.close_virtual_switch(line_id)
		# DEBUG
		#display(self, color.magenta('The real switch %d was closed.' % line_id, 'd'))
	

	def close_virtual_switch(self, line_id):
		''' Sets the 'closed' switch attribute to 'True', closing the switch.
		This operation is performed over the virtual network.
		
		Parameters
		----------
		line_id : int
			The id (in network model) of the line to be closed.
		'''

		self.virtual_network.switch['closed'][line_id] = True
		# DEBUG
		#display(self, color.magenta('The virtual switch %d was closed.' % line_id, 'd'))
		

	def is_switch_opened(self, switch_id):
		''' Returns True is the selected switch is opened. Returns False
		otherwise. This verification is performed over the real network.
		
		Parameters
		----------
		switch_id : int
			The id (in network model) of the line to be open.

		Returns
		-------
		bool
			True if the switch is opened; False otherwise.
		'''

		return self.network.switch['closed'][switch_id] == False


	def is_virtual_switch_opened(self, switch_id):
		''' Returns True is the selected switch is opened. Returns False
		otherwise. This verification is performed over the virtual network.
		
		Parameters
		----------
		switch_id : int
			The id (in network model) of the line to be open.
		
		Returns
		-------
		bool
			True if the switch is opened; False otherwise.
		'''

		return self.virtual_network.switch['closed'][switch_id] == False


	def is_switch_closed(self, switch_id):
		''' Returns True is the selected switch is closed. Returns False
		otherwise. This verification is performed over the real network.

		Parameters
		----------
		switch_id : int
			The id (in network model) of the line to be open.
		
		Returns
		-------
		bool
			True if the switch is opened; False otherwise.
		'''

		return self.network.switch['closed'][switch_id] == True
		
	
	def is_virtual_switch_closed(self, switch_id):
		''' Returns True is the selected switch is closed. Returns False
		otherwise. This verification is performed over the virtual network.

		Parameters
		----------
		switch_id : int
			The id (in network model) of the line to be open.

		Returns
		-------
		bool
			True if the switch is opened; False otherwise.
		'''

		return self.virtual_network.switch['closed'][switch_id] == True


	def set_recovery_state(self, bus_id, state):
		''' Sets the recovery state for a given bus.

		Parameters
		----------
		bus_id : int
			The id of the bus that will change its recovering state.
		state : bool
			The state to be set to the referred bus.
		'''

		if state: # i.e., if the bus is in a 'recovery' state
			self.recovery_state_of_the_buses[bus_id] += 1
		else:
			self.recovery_state_of_the_buses[bus_id] -= 1
	

	def system_recovery_state(self):
		''' Returns the system recovey state.

		Returns
		-------
		bool
			Returns True if at least one of	the BusAgent is performing its
			recovery activities. Returns False otherwise.
		'''

		for state in self.recovery_state_of_the_buses:
			if state != 0:
				return True
		return False
	

	def lock_pf(self):
		''' Lock the calculation of PF for the system.

		After this method was executed, the NetworkAgent wont be able to
		perform the PF calculation. The method 'self.unlock_pf()' releases this
		lock.
		'''

		self._pf_locker.clear()
	

	def unlock_pf(self):
		''' Unlock the calculation of PF for the system.

		This method releases the NetworkAgent to perform again the PF
		calculation.
		'''

		self._pf_locker.set()
	

	def wait_pf(self):
		''' Waits for a release PF event happens.

		This method will block the caller until a PF release event occurs. If
		the event has occurred, the caller will be free to continue its
		execution.
		'''
		self._pf_locker.wait()