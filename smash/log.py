''' This Python file groups functions related to log's writing and reading used
by the agents.
'''

from pade.acl.aid import AID
from pade.core.agent import Agent

from smash import config

import datetime


def write_log(file_name, content, path = config.LOG_PATH, time_info = True):
	if isinstance(file_name, Agent):
		file_name = file_name.aid.getName()
	elif isinstance(file_name, AID):
		file_name = file_name.getName()
	if time_info:
		dt = str(datetime.datetime.now()) + '\n'
	else:
		dt = ''
	try:
		file = open(path + file_name + '.log', 'r')
		lines = file.readlines()
		lines.append(dt + content + '\n\n')
		file = open(path + file_name + '.log', 'w')
		file.writelines(lines)
		file.close()
	except FileNotFoundError:
		file = open(path + file_name + '.log', 'w')
		file.write(dt + content + '\n\n')
		file.close()


def write_agent(agent, path = config.LOG_PATH, time_info = True):
	if time_info:
		dt = str(datetime.datetime.now()) + '\n'
	else:
		dt = ''
	try:
		file = open(path + agent.aid.getName() + '.log', 'r')
		lines = file.readlines()
		lines.append(dt + str(agent) + '\n\n')
		file = open(path + agent.aid.getName() + '.log', 'w')
		file.writelines(lines)
		file.close()
	except FileNotFoundError:
		file = open(path + agent.aid.getName() + '.log', 'w')
		file.write(dt + str(agent) + '\n\n')
		file.close()