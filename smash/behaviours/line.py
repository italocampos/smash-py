#from pade.acl.aid import AID
import pickle, uuid, color, datetime
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.behaviours.types import TickerBehaviour, CyclicBehaviour, OneShotBehaviour, SimpleBehaviour
from pade.misc.utility import display


class HealthSensor(TickerBehaviour):
	''' This behavior monitors the state of line health for LineAgent.
	'''

	def __init__(self, agent, delay):
		super().__init__(agent, delay)
		self.__fail_detected = False


	def on_tick(self):
		if not self.agent.get_line_health():	
			# Checks if the fail was detected by the first time
			if not self.__fail_detected:
				# DEBUG
				display(self.agent, color.yellow('This line has failed and will be opened.'))
				# Geting the recovery's initial time
				time = datetime.datetime.now()
				#write_log(self.agent, 'Fault detected.')
				self.__fail_detected = True
				# Requests to NetworkAgent the switch opening
				self.agent.add_behaviour(OpenSwitch(self.agent))
				# Sends to NetworkAgent the capruted time
				message = ACLMessage(ACLMessage.INFORM)
				message.set_protocol('REGISTER_TIME')
				message.set_language('LINE')
				message.set_content(pickle.dumps(time))
				message.add_receiver(self.agent.NETWORK)
				self.send(message)
		else:
			# Checks if the line was recently restored
			if self.__fail_detected:
				self.__fail_detected = False
				# DEBUG
				#write_log(self.agent, 'This line was fixed.')
				display(self.agent, color.yellow('This line was fixed but will remain opened until a explict command.'))
				# THIS IS A DEBUG TO PUT THE ENERGY BACK
				#self.agent.add_behaviour(OpenSwitch(self.agent))



class LineHealthListener(CyclicBehaviour):
	''' This behaviour listens for requests about the state of the
	line health. '''

	def action(self):
		message = self.read()
		_filter = Filter()
		_filter.set_performative(ACLMessage.REQUEST)
		_filter.set_protocol('CHECK_STATE')
		_filter.set_language('BUS')
		if _filter.filter(message):
			reply = message.create_reply()
			reply.set_performative(ACLMessage.INFORM)
			reply.set_language('LINE')
			reply.set_content(str(self.agent.get_line_health()))
			self.send(reply)



class SwitchOperationWaiter(SimpleBehaviour):
	''' This behaviour waits for the confirmation of the switching
	operations and repasses the appropriate confirmation to the
	requester BusAgent. '''

	def  __init__(self, agent, reply, tag = None):
		'''
		Parameters
		----------
		reply : ACLMessage
			The message to be sent to the requester BusAgent as reply
		tag : str, optional
			The tag used to identify the expected response from NetworkAgent
		'''

		super().__init__(agent)
		self.reply = reply
		self.tag = tag
		self._done = False
	

	def action(self):
		message = self.read()
		_filter = Filter()
		_filter.set_performative(ACLMessage.INFORM)
		_filter.set_protocol('SWITCH_OPERATION')
		_filter.set_language('LINE')
		_filter.set_conversation_id(self.tag)
		if _filter.filter(message):
			self.reply.set_content(message.get_content())
			self.reply.set_performative(ACLMessage.INFORM)
			self.send(self.reply)
			self._done = True


	def done(self):
		return self._done



class SwitchOperationListener(CyclicBehaviour):
	''' This behaviour listens for requests about switch operations.
	This receives requisitions from BusAgent and perform the
	operation in the Network.
	'''

	def action(self):
		message = self.read()
		_filter = Filter()
		_filter.set_performative(ACLMessage.REQUEST)
		_filter.set_protocol('SWITCH_OPERATION')
		_filter.set_language('BUS')
		if _filter.filter(message):
			# The ontology holds the type of operation that is requested
			net_type = message.get_ontology()
			if message.get_content() == 'OPEN':
				# Creating the tag to identify this request
				tag = str(uuid.uuid4())
				self.agent.add_behaviour(SwitchOperationWaiter(self.agent, message.create_reply(), tag))
				self.agent.add_behaviour(OpenSwitch(self.agent, tag, net_type))
			elif message.get_content() == 'CLOSE':
				# Creating the tag to identify this request
				tag = str(uuid.uuid4())
				self.agent.add_behaviour(SwitchOperationWaiter(self.agent, message.create_reply(), tag))
				self.agent.add_behaviour(CloseSwitch(self.agent, tag, net_type))



class OpenSwitch(OneShotBehaviour):
	''' This behaviour sends a message to the NetworkAgent requesting
	the opening of the switch corresponding to this LineAgent. '''

	def __init__(self, agent, tag = None, net_type = 'real'):
		'''
		Parameters
		----------
		agent : BusAgent
			The BusAgent that holds this behaviour.
		tag : str, optional
			The tag to mark the opening request to be sent.
		net_type : str, optional
			Specifies over which network the opening operation will be
			requested (real or virtual network).
		'''

		super().__init__(agent)
		self.tag = tag
		self.net_type = net_type


	def action(self):
		message = ACLMessage(ACLMessage.REQUEST)
		message.set_protocol('SWITCH_OPERATION')
		message.set_language('LINE')
		# Puts the id of this agent on the network
		message.set_content(pickle.dumps({
			'operation': 'OPEN',
			'switch_id': self.agent.id,
			'net_type': self.net_type,
			}))
		message.add_receiver(self.agent.NETWORK)
		message.set_conversation_id(self.tag)
		self.send(message)



class CloseSwitch(OneShotBehaviour):
	''' This behaviour sends a message to the NetworkAgent requesting
	the closing of the switch corresponding to this LineAgent. '''

	def __init__(self, agent, tag = None, net_type = 'real'):
		'''
		Parameters
		----------
		agent : BusAgent
			The BusAgent that holds this behaviour.
		tag : str, optional
			The tag to mark the closing request to be sent.
		net_type : str, optional
			Specifies over which network the closing operation will be
			requested (real or virtual network).
		'''

		super().__init__(agent)
		self.tag = tag
		self.net_type = net_type


	def action(self):
		message = ACLMessage(ACLMessage.REQUEST)
		message.set_protocol('SWITCH_OPERATION')
		message.set_language('LINE')
		# Puts the id of this agent on the network
		message.set_content(pickle.dumps({
			'operation': 'CLOSE',
			'switch_id': self.agent.id,
			'net_type': self.net_type,
			}))
		message.add_receiver(self.agent.NETWORK)
		message.set_conversation_id(self.tag)
		self.send(message)