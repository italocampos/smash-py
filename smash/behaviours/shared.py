from pade.behaviours.types import SimpleBehaviour
from pade.acl.messages import ACLMessage
from pade.misc.utility import display


class HandleMessage(SimpleBehaviour):
	''' This behaviour is a general request-response behaviour.

	This behaviour can be used to send a request and wait for the responses.
	The required parameters are the template of the request message and the
	filter used to filter the responses.

	Attributes
	----------
	message_template : ACLMessage
		The template of the message to be sent.
	filter_template : Filter
		The filter used to filter the received responses.
	requested : bool
		The bool that sinalizes if the requests are sent.
	_done : bool
		The bool that sinalizes when the behaviour ends.
	requests : int
		The counter of the requests that were sent.
	'''

	def __init__(self, agent, message_template, filter_template):
		'''
		Parameters
		----------
		agent : Agent
			The agent that holds the behaviour.
		message_template : ACLMessage
			The template of the message to be sent.
		filter_template : Filter
			The filter used to filter the received responses.
		'''

		super().__init__(agent)
		self.message_template = message_template
		self.filter_template = filter_template
		self.requested = False
		self._done = False
		self.requests = len(message_template.receivers)
		self.data = list()


	def action(self):
		if not self.requested:
			self.send(self.message_template)
			self.requested = True
		message = self.read()
		if self.filter_template.filter(message):
			self.data.append(message.clone())
			self.requests -= 1
		if self.requests == 0:
			self.set_return(self.data)
			self._done = True


	def done(self):
		return self._done