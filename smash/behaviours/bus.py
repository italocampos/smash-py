import uuid, pickle, threading, math, color
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.behaviours.types import TickerBehaviour, SimpleBehaviour, CyclicBehaviour
from pade.misc.utility import display
from smash.behaviours import shared, functions


class EnergySensor(TickerBehaviour):
	''' This behavior models the power sensor of the bus, monitoring the
	corresponding value in the control file.
	'''
	
	def __init__(self, agent, delay):
		super().__init__(agent, delay)
		self._fault_detected = False

	def on_tick(self):
		if self.agent.has_energy_supply():
			# Checks if the power supply was recently restored
			if self._fault_detected:
				# DEBUG
				display(self.agent, color.green('Power supply restored.'))
				#write_log(self.agent, 'Power supply restored.')
				self._fault_detected = False
		else:
			# Checks if the fault was detected by the first time
			if not self._fault_detected:
				# DEBUG
				display(self.agent, color.red('Fault detected.'))
				#write_log(self.agent, 'Fault detected.')
				self._fault_detected = True
				# Start the self-healing process here
				self.agent.add_behaviour(CheckNeighborhood(self.agent))



class CheckNeighborhood(SimpleBehaviour):
	''' This behavior checks the neighbors BusAgent (looking for 
	energized buses) and the LineAgent (looking for tie lines) searching
	for help when there's a power fault.
	'''

	def __init__(self, agent):
		super().__init__(agent)
		self._requested = False
		self._requests = 0
		self._done = False
		self._tags = list()
		self._tests = [0 for _ in self.agent.neighbors]


	def action(self):
		if not self._requested:
			# Sends the messages to each pair bus-line
			for neighbor in self.agent.neighbors:
				message = ACLMessage(ACLMessage.REQUEST)
				message.set_protocol('CHECK_STATE')
				message.set_language('BUS')
				# This tag will identify the requests according the order of
				# neighbors in the self.agent.neighbors list
				self._tags.append(str(uuid.uuid4()))
				message.set_conversation_id(self._tags[-1])
				message.add_receiver(neighbor['bus_aid'])
				message.add_receiver(neighbor['line_aid'])
				self._requests += 2
				self.send(message)
			self._requested = True
		# Checking if the agent still waits for answers
		if self._requests > 0:
			# Filtering the responses
			_filter = Filter()
			_filter.set_performative(ACLMessage.INFORM)
			_filter.set_protocol('CHECK_STATE')
			message = self.read()
			if _filter.filter(message) and message.get_conversation_id() in self._tags:
				# Gets the tag index of the neighbor that sent the response
				index = self._tags.index(message.get_conversation_id())
				# If the neighbor (bus or line) said True, it can help this bus
				if message.get_content() == 'True':
					self._tests[index] += 1
				self._requests -= 1
		else:
			connection_points = list()
			for i in range(len(self._tests)):
				# If both bus and line said 'True'
				if self._tests[i] == 2:
					connection_points.append(self.agent.neighbors[i]['bus_aid'])
					# DEBUG
					#display(self.agent, 'This agent possibly can help me: %s' % self.agent.neighbors[i]['bus_aid'].getName())
				#else:
					# DEBUG
					#display(self.agent, 'This agent can not help me: %s' % self.agent.neighbors[i]['bus_aid'].getName())
			# If there is at least one connection point, then start the recovery process
			if connection_points != []:
				# > Starts the recovery process
				self.agent.add_behaviour(Recover(self.agent, connection_points))
			self._done = True


	def done(self):
		return self._done



class CheckNeighborhoodListener(CyclicBehaviour):
	''' This class listen for requests about the state of neighborhood,
	process started when the fault is detected.
	'''

	def action(self):
		_filter = Filter()
		_filter.set_performative(ACLMessage.REQUEST)
		_filter.set_protocol('CHECK_STATE')
		_filter.set_language('BUS')
		message = self.read()
		if _filter.filter(message):
			reply = message.create_reply()
			reply.set_performative(ACLMessage.INFORM)
			reply.set_content(str(self.agent.has_energy_supply()))
			self.send(reply)



class Recover(SimpleBehaviour):
	''' This behavior starts the recovery process. It is called
	by BusAgent that has at least one recovery point in 
	network. It checks for other possible links with other
	BusAgent and, at the end, connect to the recovery point, if
	it is possible.
	'''
	
	def __init__(self, agent, connection_points):
		super().__init__(agent)
		self._connection_points = connection_points
		self._tag = None
		self._done = False
		self._started = False
		self._degree_requests = 0
		self._line_requests = 0
		self._working_lines = list()
		self._neighbor_degrees = list()
	

	def action(self):
		# > Requesting to NetworkAgent the PF for connection points
		if not self._started:
			# >> Fetching the index of the lines that possibly can be used with
			# the connection points
			lines_to_close = list()
			lines_to_open = list()
			for point in self._connection_points:
				neighbor = self.agent.get_neighbor(point)
				# >>> The lines_to_close list will indicate the order of
				# analysis for the connection points 
				lines_to_close.append(neighbor['line_id'])
			# >> This list saves the configs to request the PF
			configurations = list()
			# >> This 'for' builds the proper network configuration for each
			# available connection point
			for line in lines_to_close:
				# >> Fetching the index of lines that must be opened so that
				# the power flow can be correctly calculated for the current
				# network configuration
				lto = list()
				for neighbor in self.agent.neighbors:
					if line != neighbor['line_id']:
						lto.append(neighbor['line_id'])
				lines_to_open.append(lto.copy())
				configurations.append({
					'lines_to_close': [line],
					'lines_to_open': lto,
					'reference_bus': self.agent.id,
				})
			# >> This section of code requests the PF to NetworkAgent for each
			# available connection point.
			message = ACLMessage(ACLMessage.REQUEST)
			# >>> Creating a tag for the message
			tag = str(uuid.uuid4())
			message.set_conversation_id(tag)
			message.set_protocol('POWER_FLOW')
			message.set_language('BUS')
			message.set_ontology('virtual')
			message.add_receiver(self.agent.NETWORK)
			message.set_content(pickle.dumps(configurations))
			# >>> Creating the filter for the expected responses
			_filter = Filter()
			_filter.set_performative(ACLMessage.INFORM)
			_filter.set_protocol('POWER_FLOW')
			_filter.set_language('BUS')
			_filter.set_ontology('virtual')
			_filter.set_conversation_id(tag)
			# >>> Requesting the PF to NetworkAgent and waiting the response.
			waiter = shared.HandleMessage(self.agent, message, _filter)
			self.agent.add_behaviour(waiter)
			# >>> Getting just the first response because only one requisition was
			# sent to NetworkAgent
			response = self.wait_return(waiter)[0]
			content = pickle.loads(response.get_content())

			# > Creating the list of voltage_values for each connection point
			voltage_values = list()
			for result in content:
				voltage_values.append(result['voltage'] if result['result'] else 0.0)

			# > Getting the best voltage profile found
			best_voltage = max(voltage_values)
			if best_voltage == 0.0:
				display(self.agent, "I can't connect with any neighbor because of network constraints.")
				# > Disconfirm the usage of the configuration
				functions.disconfirm_topology_usage(self.agent)
			else:
				# > Getting the relevant information about the selection of the
				# connection point
				# >> Gets the position index of the best voltage profile found
				selected_index = voltage_values.index(best_voltage)
				# >> Gets the AID of the line selected
				[selected_line] = functions.convert_into_aid(self.agent, [lines_to_close[selected_index]])
				# >> Gets the AID of the neighbor selected
				selected_neighbor = functions.get_neighbor_of_line(self.agent, selected_line)
				
				# > Requesting the switching operations
				opening_request = ACLMessage(ACLMessage.REQUEST)
				opening_request.set_protocol('SWITCH_OPERATION')
				opening_request.set_language('BUS')
				# >> Making the operations in the virtual network
				opening_request.set_ontology('virtual')

				# >> Requesting the switches opening
				opening_request.set_content('OPEN')
				# >> Getting the AID of the lines to be opened
				for line in functions.convert_into_aid(self.agent, lines_to_open[selected_index]):
					opening_request.add_receiver(line)
					# DEBUG
					#display(self.agent, 'Sending ' + color.yellow('opening') + ' request to %s.' % color.blue(line.getName()))
				# >> Waiting opening operation confirmations
				# >>> Creating filter template for the expcted responses
				_filter = Filter()
				_filter.set_performative(ACLMessage.INFORM)
				_filter.set_protocol('SWITCH_OPERATION')
				_filter.set_language('BUS')
				_filter.set_ontology('virtual')
				# >>> Instantiating the waiter behaviour
				waiter = shared.HandleMessage(self.agent, opening_request, _filter)
				self.agent.add_behaviour(waiter)
				# >>>> This behaviour will wait the finalization of the
				# 'waiter' behaviour. The 'waiter' returning means that the
				# switchig operations was performed successfully. The return
				# content will be disconsidered because the NetworkAgent always
				# replies with a SUCCESS message.
				self.wait_return(waiter)

				# >> Requesting the switches closing
				closing_request = ACLMessage(ACLMessage.REQUEST)
				closing_request.set_protocol('SWITCH_OPERATION')
				closing_request.set_language('BUS')
				closing_request.set_ontology('virtual')
				closing_request.set_content('CLOSE')
				closing_request.add_receiver(selected_line)
				# DEBUG
				#display(self.agent, 'Sending ' + color.black('closing', 'b') + ' request to %s.' % color.blue(line.getName()))
				# >>> Waiting closing operation confirmations
				waiter_ = shared.HandleMessage(self.agent, closing_request, _filter)
				self.agent.add_behaviour(waiter_)
				self.wait_return(waiter_)

				# > Confirm the usage of the configuration to NetworkAgent
				functions.confirm_topology_usage(self.agent)
				display(self.agent, 'I have %s to the neighbor %s.' % (color.green('CONNECTED'), color.blue(selected_neighbor['bus_aid'].getName())))
				# > Setting the recovery state for this agent as `True`
				functions.send_recovery_state(self.agent, True)
				# > Writing the connection with the connection point (other
				# BusAgent) as a proposal being analyzed
				self.agent.current_proposal.write({
					'proposer': selected_neighbor['bus_aid'],
					'voltage': best_voltage,
				})
				# DEBUG
				#display(self.agent, color.magenta('WRITING the analyzed proposal field with', 'd') + ' %s.' % self.agent.current_proposal.read())

				# > Checking the states of the lines to other neighbors
				# (starting the recursive proposition)
				line_request = ACLMessage(ACLMessage.REQUEST)
				line_request.set_protocol('CHECK_STATE')
				line_request.set_language('BUS')
				self._tag = str(uuid.uuid4())
				line_request.set_conversation_id(self._tag)
				# >> Getting the lines aid
				for aid in functions.convert_into_aid(self.agent, lines_to_open[selected_index]):
					line_request.add_receiver(aid)
					self._line_requests += 1
					# DEBUG
					#display(self.agent, 'Sending the line check to %s.' % aid.getName())
				if line_request.get_receivers() == []:
					# > There is no lines or neighors to check. The recovery
					# process for this agent must be finalized.
					self.agent.add_behaviour(WaitConfirmations(self.agent, self.agent.current_proposal.read(), []))
				else:
					self.send(line_request)
			
			# > Indicates that the recovery algorithm has started
			self._started = True

		# > Handling the LineAgent responses
		elif self._line_requests > 0:
			_filter = Filter()
			_filter.set_performative(ACLMessage.INFORM)
			_filter.set_protocol('CHECK_STATE')
			_filter.set_language('LINE')
			_filter.set_conversation_id(self._tag)
			message = self.read()
			if _filter.filter(message):
				if message.get_content() == 'True':
					self._working_lines.append(message.get_sender())
				self._line_requests -= 1
			if self._line_requests == 0:
				# DEBUG
				#display(self.agent, 'I have got the responses from the line check requisitions.')
				if self._working_lines == []:
					# > There is no lines working around. The recovery process
					# for this agent must be finalized.
					self.agent.add_behaviour(WaitConfirmations(self.agent, self.agent.current_proposal.read(), []))
				else:
					# > Requesting the degree of the neighbors
					degree_request = ACLMessage(ACLMessage.REQUEST)
					degree_request.set_protocol('DEGREE_REQUEST')
					degree_request.set_language('BUS')
					self._tag = str(uuid.uuid4())
					degree_request.set_conversation_id(self._tag)
					# >> Adding as receivers the neighbors with good lines
					for aid in self._working_lines:
						a = functions.get_neighbor_of_line(self.agent, aid)['bus_aid']
						degree_request.add_receiver(a)
						self._degree_requests += 1
					self.send(degree_request)

		# > Handling the BusAgent responses about degrees
		elif self._degree_requests > 0:
			_filter = Filter()
			_filter.set_performative(ACLMessage.INFORM)
			_filter.set_protocol('DEGREE_REQUEST')
			_filter.set_language('BUS')
			_filter.set_conversation_id(self._tag)
			message = self.read()
			if _filter.filter(message):
				# >> Adding the neighbor degrees in a list
				self._neighbor_degrees.append({
					'bus_aid': message.get_sender(),
					'degree': int(message.get_content()),
				})
				self._degree_requests -= 1
			if self._degree_requests == 0:
				# > Finalizing the recovery sending in other behaviour
				self.agent.add_behaviour(WaitConfirmations(self.agent, self.agent.current_proposal.read(), self._neighbor_degrees))

		# Finalizes the behaviour
		else:
			self._done = True


	def done(self):
		return self._done



class DegreeRequestListener(CyclicBehaviour):
	''' This behavior listens for degree requisitions and answers
	with the bus degree on network. '''

	def action(self):
		_filter = Filter()
		_filter.set_performative(ACLMessage.REQUEST)
		_filter.set_protocol('DEGREE_REQUEST')
		_filter.set_language('BUS')
		message = self.read()
		if _filter.filter(message):
			reply = message.create_reply()
			reply.set_performative(ACLMessage.INFORM)
			reply.set_content(str(self.agent.num_neighbors()))
			self.send(reply)



class RecoveryListener(CyclicBehaviour):
	''' This behavior listens for recover proposes and answers
	according with the network state. This behaviour requests for
	NetworkAgent PF calculation and checks wether is possible to make
	the connection or not. 
	
	Properties
	----------
	recent_proposals : list
		Saves the recent proposals made by neighbor BusAgents. The elements of
		the list are dicts with the following format:
		{'proposer': AID,
		'datetime': datetime.datetime}
	IMPROVEMENT_RATE : float
		The minimum rate of improvement to be beat by a new proposal to be
		accepted as a interesting connection.
		'''

	def __init__(self, agent):
		super().__init__(agent)
		self.recent_proposals = list()
		self.IMPROVEMENT_RATE = 0


	def action(self):
		# > Handling the received recovery proposals
		message = self.read()
		_filter = Filter()
		_filter.set_performative(ACLMessage.PROPOSE)
		_filter.set_protocol('RECOVERY')
		_filter.set_language('BUS')
		if _filter.filter(message):
			# DEBUG
			display(self.agent, 'I received a recovery proposal from %s.' % color.blue(message.get_sender().getName()))
			# > Checks if the received proposal is the most recent sent by the
			# sender. This avoids incosistent data.
			if self.is_the_most_recent_proposal(message):
				# > Geting the resulting voltage profile at this bus when the
				# energy flow comes through the proposer
				proposed_voltage = self.request_power_flow(message.get_sender())
				proposed_voltage = pickle.loads(proposed_voltage.get_content())[0]
				# > Checks if the proposed connection is feasible.
				if proposed_voltage['result']:
					# > Gets the proposed voltage value in numpy.float64 format
					proposed_voltage = proposed_voltage['voltage']
					# > Avoiding 'nan' responses
					proposed_voltage = 0.0 if math.isnan(proposed_voltage) else proposed_voltage
					# Thread-safe reading process to this resource (property)
					current_proposal = self.agent.current_proposal.read()
					# > Checks if another proposal already is being analyzed.
					if current_proposal != None:
						# > Checking if the proposed voltage improves the
						# voltage in the previous proposal according the
						# provided improvement rate.
						if proposed_voltage > current_proposal['voltage'] * (1 + self.IMPROVEMENT_RATE):
							# DEBUG
							display(self.agent, 'The connection with {bus} improves the voltage currently analyzed at {n}. '.format(
									bus = color.blue(message.get_sender().getName()),
									n = color.yellow(str(round(((proposed_voltage/current_proposal['voltage'])-1) * 100, ndigits = 1)) + '%', 'bold'),
								) +
								'This connection will be accepted and the other will be undone.')
							# DEBUG
							#display(self.agent, 'Previous voltage profile: %f; Proposed voltage: %f.' % (voltage_profile, proposed_voltage))
							# > Accepting the new proposal
							self.accept_proposal(message, proposed_voltage)
						# DEBUG
						else:
							# DEBUG
							display(self.agent, 'The voltage profile proposed by %s ' % color.blue(message.get_sender().getName()) +
								"doesn't improve the current voltage profile that have been analyzed. Refusing the connection.")
							# > Disconfirming the configuration to NetworkAgent
							functions.disconfirm_topology_usage(self.agent)
							# > Refusing the connection with the proposer
							reply = message.create_reply()
							reply.set_performative(ACLMessage.REJECT_PROPOSAL)
							self.send(reply)
					# > Case there is no proposes being analyzed
					else:
						# > Checks if the proposal improves the local voltage
						# profile
						# >> Getting the current voltage profile for this bus
						# (the NetworkAgent holds this information)
						# >>> Creating the request to be sent
						request = ACLMessage(ACLMessage.REQUEST)
						tag = str(uuid.uuid4())
						request.set_conversation_id(tag)
						request.set_protocol('BUS_DATA')
						request.set_language('BUS')
						request.set_ontology('virtual')
						request.add_receiver(self.agent.NETWORK)
						request.set_content(pickle.dumps({
							'parameter': 'voltage',
							'bus_id': self.agent.id,
						}))
						# >>> Building the filter for the expected response
						_filter = Filter()
						_filter.set_performative(ACLMessage.INFORM)
						_filter.set_protocol('BUS_DATA')
						_filter.set_language('BUS')
						_filter.set_ontology('virtual')
						_filter.set_conversation_id(tag)
						# >>> Instantiating a behaviour to send and wait the
						# responses
						handler = shared.HandleMessage(self.agent, request, _filter)
						self.agent.add_behaviour(handler)
						# >>> Waiting and getting the return from NetworkAgent
						data = self.wait_return(handler)
						voltage_profile = float(data[0].get_content())
						voltage_profile = 0.0 if math.isnan(voltage_profile) else voltage_profile
						# >>> Checking if the proposed voltage improves the
						# current voltage profile, according the provided 
						# improvement rate. 
						if proposed_voltage > voltage_profile * (1 + self.IMPROVEMENT_RATE):
							# DEBUG
							text = ''
							if voltage_profile == 0.0:
								text = 'The connection with %s will recover this bus. Accepting the connection.' % color.blue(message.get_sender().getName())
							else:
								text = 'The connection with {bus} improves the current voltage profile at {n}. The connection will be accepted.'.format(
									bus = color.blue(message.get_sender().getName()),
									n = color.yellow(str(round(((proposed_voltage/voltage_profile)-1) * 100, ndigits = 1)) + '%', 'bold'),
								)
							display(self.agent, text)
							#display(self.agent, 'The current voltage profile is %f and the proposed is %f.' % (voltage_profile, proposed_voltage))
							# > Accepting the proposal
							self.accept_proposal(message, proposed_voltage)
						# DEBUG
						else:
							# > The proposal doesn't improve (or the 
							# improvement is irrelevant) the current proposal
							# DEBUG
							#display(self.agent, 'Local voltage: %f; proposed_voltage: %f.' % (voltage_profile, proposed_voltage))
							display(self.agent, "The proposal from %s doesn't improve the local voltage profile. Refusing the connection." % color.blue(message.get_sender().getName()))
							# > Disconfirm the configuration to NetworkAgent
							functions.disconfirm_topology_usage(self.agent)
							reply = message.create_reply()
							reply.set_performative(ACLMessage.REJECT_PROPOSAL)
							self.send(reply)
				else:
					# > Disconfirm the configuration to NetworkAgent
					functions.disconfirm_topology_usage(self.agent)
					# > Rejecting the proposal due to the network constraints
					reply = message.create_reply()
					reply.set_performative(ACLMessage.REJECT_PROPOSAL)
					self.send(reply)
					# DEBUG
					display(self.agent, 'The connection with %s will violate a network constraint. Refusing this connection.' % color.blue(message.get_sender().getName()))
			else:
				# > Rejecting the connection due to the obsolete data
				# > Disconfirm the configuration to NetworkAgent
				functions.disconfirm_topology_usage(self.agent)
				reply = message.create_reply()
				reply.set_performative(ACLMessage.REJECT_PROPOSAL)
				self.send(reply)
				# DEBUG
				display(self.agent, 'This proposal is obsolete. Refusing the connection with %s.' % color.blue(message.get_sender().getName()))

	
	def request_power_flow(self, proposer):
		'''
		Requests and return the power flow data

		This method requests, waits and returns the results of the power flow,
		considering a connection through the proposer bus agent.

		Parameters
		----------
		proposer : AID
			The AID of the proposer (bus).
		
		Retuns
		----------
		dict
			The data returned by the NetworkAgent containing the PF results.
		'''

		# Getting the index of the lines that must be opened before the PF
		lines_to_open = list()
		for neighbor in self.agent.neighbors:
			if neighbor['bus_aid'] != proposer:
				lines_to_open.append(neighbor['line_id'])
		# Building the PF request message to NetworkAgent
		request = ACLMessage(ACLMessage.REQUEST)
		# Generating a tag for the request
		tag = str(uuid.uuid4())
		request.set_conversation_id(tag)
		request.set_protocol('POWER_FLOW')
		request.set_language('BUS')
		request.set_ontology('virtual')
		request.add_receiver(self.agent.NETWORK)
		request.set_content(pickle.dumps([{
			'lines_to_close': [self.agent.get_neighbor(proposer)['line_id']],
			'lines_to_open': lines_to_open,
			'reference_bus': self.agent.id,
		}]))
		# Building the filter for the expected response
		_filter = Filter()
		_filter.set_performative(ACLMessage.INFORM)
		_filter.set_protocol('POWER_FLOW')
		_filter.set_language('BUS')
		_filter.set_ontology('virtual')
		_filter.set_conversation_id(tag)
		# Instantiating a new behaviour to send and wait the response from the
		# NetworkAgent with the PF results.
		handler = shared.HandleMessage(self.agent, request, _filter)
		self.agent.add_behaviour(handler)
		data = self.wait_return(handler)
		
		return data[0]


	def is_the_most_recent_proposal(self, message):
		'''current_proposal
		Checks if a received proposal is the most recent

		This method checks the local data to evaluate if the provided message
		is the most recent proposal from this sender. If there is no entry for
		the sender in the list, this method adds a new entry and returns True.

		Parameters
		----------
		message : ACLMessage
			The message that contains the analyzed proposal.
		
		Retuns
		----------
		bool
			Returns True if the provided proposal is the most recent or whether
			there is no entry for the proposal in the behaviour list. Returns
			False othehrwise.
		'''

		for proposal in self.recent_proposals:
			if message.get_sender() == proposal['proposer']:
				if message.get_datetime() > proposal['datetime']:
					proposal.update(
						{'proposer': message.get_sender(),
						'datetime': message.get_datetime()})
					return True
				else:
					return False
		self.recent_proposals.append(
			{'proposer': message.get_sender(),
			'datetime': message.get_datetime()})
		return True
						
	
	def accept_proposal(self, proposal, proposed_voltage):
		''' Performs all the proceeds to connect this BusAgent with the
		provided neighbor.

		This function closes the virtual switch to `neighbor`, replies to the
		provided `neighbor` the connection acceptance, and performs the checks
		to make new proposals to the other neighbors. This function assumes
		that the required verification have been made. This function was
		written because of code reuse.

		Parameters
		----------
		proposal : ACLMessage
			The proposal sent by the proposer neighbor.
		proposed_voltage : numpy.float64
			The calculated voltage for the provided proposal.
		'''

		# > Saves the proposal as the accepted and current proposal
		self.agent.current_proposal.write({
			'proposer': proposal.get_sender(),
			'voltage': proposed_voltage,
		})
		# DEBUG
		#display(self.agent, color.magenta('WRITING the analyzed proposal field with', 'd') + ' %s.' % self.agent.current_proposal.read())

		# > Setting the recovery state of this bus as 'True'
		functions.send_recovery_state(self.agent, True)

		# > Connecting with proposer
		# >> Opening all the nearby lines
		opening_request = ACLMessage(ACLMessage.REQUEST)
		opening_request.set_protocol('SWITCH_OPERATION')
		opening_request.set_language('BUS')
		# >>> Making the operations in the virtual network
		opening_request.set_ontology('virtual')
		opening_request.set_content('OPEN')
		# >>>> Getting the aid of the lines to be opened
		for neighbor in self.agent.neighbors:
			if neighbor['bus_aid'] != proposal.get_sender():
				opening_request.add_receiver(neighbor['line_aid'])
				# DEBUG
				#display(self.agent, color.magenta('Opening the virtual switch at %s.' % neighbor['line_aid'].getName(), 'd'))
				#display(self.agent, 'Sending ' + color.yellow('opening') + ' request to %s.' % color.blue(neighbor['line_aid'].getName()))
		_filter = Filter()
		_filter.set_performative(ACLMessage.INFORM)
		_filter.set_protocol('SWITCH_OPERATION')
		_filter.set_language('BUS')
		_filter.set_ontology('virtual')
		# >>> Only opens the lines if there is at least one line to be opened
		if opening_request.get_receivers() != []:	
			waiter = shared.HandleMessage(self.agent, opening_request, _filter)
			self.agent.add_behaviour(waiter)
			self.wait_return(waiter)

		# >> Closing the line to the proposer
		closing_request = ACLMessage(ACLMessage.REQUEST)
		closing_request.set_protocol('SWITCH_OPERATION')
		closing_request.set_language('BUS')
		closing_request.set_ontology('virtual')
		closing_request.set_content('CLOSE')
		closing_request.add_receiver(self.agent.get_neighbor(proposal.get_sender())['line_aid'])
		# DEBUG
		#display(self.agent, color.magenta('Closing the virtual switch at %s.' % closing_request.receivers[0].getName(), 'd'))
		#display(self.agent, 'Sending ' + color.black('closing', 'b') + ' request to %s.' % color.blue(closing_request.receivers[0].getName()))
		waiter_ = shared.HandleMessage(self.agent, closing_request, _filter)
		self.agent.add_behaviour(waiter_)
		self.wait_return(waiter_)

		# > Confirming the usage of the configuration to NetworkAgent
		functions.confirm_topology_usage(self.agent)

		# > Notifying the proposer about the acceptance of connection
		acceptance = proposal.create_reply()
		acceptance.set_performative(ACLMessage.ACCEPT_PROPOSAL)
		self.send(acceptance)

		# > Checks the health of the lines to other neighbors
		line_health_request = ACLMessage(ACLMessage.REQUEST)
		line_health_request.set_protocol('CHECK_STATE')
		line_health_request.set_language('BUS')
		tag = str(uuid.uuid4())
		line_health_request.set_conversation_id(tag)
		# >> Getting the aid of nearby lines, except for the
		# line between this agent and the proposer agent
		for neighbor in self.agent.neighbors:
			if neighbor['bus_aid'] != proposal.get_sender():
				line_health_request.add_receiver(neighbor['line_aid'])
		
		# >> Continues only if there is at least one line to check its health
		if line_health_request.get_receivers() != []:
			# >> Creating a filter to the expected format of response
			_filter = Filter()
			_filter.set_performative(ACLMessage.INFORM)
			_filter.set_protocol('CHECK_STATE')
			_filter.set_language('LINE')
			_filter.set_conversation_id(tag)
			# >> Requesting the health state of the lines and waiting the responses
			handler = shared.HandleMessage(self.agent, line_health_request, _filter)
			self.agent.add_behaviour(handler)
			responses = self.wait_return(handler)
			# >> Selecting only the lines that can be used
			working_lines = list()
			for response in responses:
				if response.get_content() == 'True':
					working_lines.append(response.get_sender())

			# > Selecting the neighbors by the degree
			# >> Continues only if there is at least one working lines available
			if working_lines != []:
				# >> Preparing the degree request
				degree_request = ACLMessage(ACLMessage.REQUEST)
				degree_request.set_protocol('DEGREE_REQUEST')
				degree_request.set_language('BUS')
				tag = str(uuid.uuid4())
				degree_request.set_conversation_id(tag)
				# >> Adding the neighbors as receivers (this is made by using the
				# corresponding line AIDs).
				for line in working_lines:
					degree_request.add_receiver(functions.get_neighbor_of_line(self.agent, line)['bus_aid'])
				# >> Creating a filter to the expected response format
				_filter = Filter()
				_filter.set_performative(ACLMessage.INFORM)
				_filter.set_protocol('DEGREE_REQUEST')
				_filter.set_language('BUS')
				_filter.set_conversation_id(tag)
				# >> Requesting the degree of the neighbor buses
				handler = shared.HandleMessage(self.agent, degree_request, _filter)
				self.agent.add_behaviour(handler)
				responses = self.wait_return(handler)
				# >> Puting the received response under a dict form
				neighbor_degrees = list()
				for response in responses:
					neighbor_degrees.append({
						'bus_aid': response.get_sender(),
						'degree': int(response.get_content()),
					})
				# >> Sending the proposals following the degree descending order
				self.agent.add_behaviour(WaitConfirmations(self.agent, self.agent.current_proposal.read(), neighbor_degrees))
				# DEBUG
				#display(self.agent, 'The degrees of nearby buses are: %s.' % neighbor_degrees)
			else:
				# If there is no working lines, deactivate the recovery state
				# and clean the analyzed proposal field
				functions.send_recovery_state(self.agent, False)
				self.agent.current_proposal.write(None)
				# DEBUG
				#display(self.agent, color.magenta('CLEANING the analyzed proposal field.', 'd'))
		else:
			# If there is no lines to verify, deactivate the recovery state and
			# clean the analyzed proposal field
			functions.send_recovery_state(self.agent, False)
			self.agent.current_proposal.write(None)
			# DEBUG
			#display(self.agent, color.magenta('CLEANING the analyzed proposal field.', 'd'))



class WaitConfirmations(SimpleBehaviour):
	''' This behaviour is called to finish recovery proposals after this agent
	accepted to connect (virtually) with other BusAgent. This behaviour also
	notifies the NetworkAgent about the end of the recovery process for this
	agent.
	
	Properties
	----------
	neighbor_degrees : dict
		A dict containing the addresses of the neighbors able to receive the
		proposals and their respective degrees in the network. The dict must
		have the following format:
		{'bus_aid': AID,
		'degree': int}
	analyzed_proposal : dict
		The proposal being analyzed. The proposal must have the following
		format:
		{'proposer': AID,
		'voltage:': numpy.float64}
	_done : bool
		Indicates when the behaviour will end.
	'''

	def __init__(self, agent, analyzed_proposal, neighbor_degrees):
		'''
		Parameters
		----------
		agent : smash.BusAgent
			The BusAgent that holds this behaviour.
		analyzed_proposal : dict
			The proposal being analyzed. The proposal must have the following
			format:
			{'proposer': AID,
			'voltage:': numpy.float64}
		neighbor_degrees : dict
			A dict containing the addresses of the neighbors able to receive
			the proposals and their respective degrees in the network. The dict
			must have the following format:
			{'bus_aid': AID,
			'degree': int}
		'''
		
		super().__init__(agent)
		self.neighbor_degrees = neighbor_degrees
		self.analyzed_proposal = analyzed_proposal
		self._done = False
	

	def action(self):
		while self.neighbor_degrees != []:
			# > Checking if the this bus still is a supplied bus
			request = ACLMessage(ACLMessage.REQUEST)
			tag = str(uuid.uuid4())
			request.set_conversation_id(tag)
			request.set_protocol('BUS_DATA')
			request.set_language('BUS')
			request.set_ontology('virtual')
			request.add_receiver(self.agent.NETWORK)
			request.set_content(pickle.dumps({
				'parameter': 'supply',
				'bus_id': self.agent.id,
			}))
			# >> Building the filter for the expected response
			_filter = Filter()
			_filter.set_performative(ACLMessage.INFORM)
			_filter.set_protocol('BUS_DATA')
			_filter.set_language('BUS')
			_filter.set_ontology('virtual')
			_filter.set_conversation_id(tag)
			# >> Instantiating a behaviour to send and wait the responses
			handler = shared.HandleMessage(self.agent, request, _filter)
			self.agent.add_behaviour(handler)
			# >> Waiting and getting the return from NetworkAgent
			response = self.wait_return(handler)[0]
			# >> Checking the response and also checks if the current analyzed
			# haven't changed. Continue sending the recovery proposal only if
			# these two criterias are True
			if response.get_content() == 'True' and self.agent.current_proposal.read() == self.analyzed_proposal:
				# > Getting the neighbor with the max degree and removing it
				# from the `neighbor_degree` list
				neighbor = functions.max_degree(self.neighbor_degrees)
				proposal = ACLMessage(ACLMessage.PROPOSE)
				proposal.set_protocol('RECOVERY')
				proposal.set_language('BUS')
				tag = str(uuid.uuid4())
				proposal.set_conversation_id(tag)
				proposal.add_receiver(neighbor['bus_aid'])
				_filter = Filter()
				_filter.set_protocol('RECOVERY')
				_filter.set_language('BUS')
				_filter.set_conversation_id(tag)
				# > Sending the proposal and waiting the responses
				handler = shared.HandleMessage(self.agent, proposal, _filter)
				self.agent.add_behaviour(handler)
				# DEBUG
				#display(self.agent, color.magenta("I'm waiting for the response of %s" % neighbor['bus_aid'].getName()))
				response = self.wait_return(handler)[0]
				if response.get_performative() == ACLMessage.ACCEPT_PROPOSAL:
					# DEBUG
					display(self.agent, '%s %s the conection.' % (color.blue(response.get_sender().getName()), color.green('ACCEPTED')))
					# > Checking if the analyzed proposal don't have changed
					if self.agent.current_proposal.read() != self.analyzed_proposal:
						# > Opening the switch to the respondent
						# DEBUG
						display(self.agent, 'The acception of %s is not more valid. Undoing the connection.' % color.blue(response.get_sender().getName()))
						opening_request = ACLMessage(ACLMessage.REQUEST)
						opening_request.set_protocol('SWITCH_OPERATION')
						opening_request.set_language('BUS')
						opening_request.set_ontology('virtual')
						opening_request.set_content('OPEN')
						# >> Getting the aid of the line to be opened
						opening_request.add_receiver(functions.get_line_to_neighbor(self.agent, response.get_sender()))
						# Waiting switching operation confirmation
						_filter = Filter()
						_filter.set_performative(ACLMessage.INFORM)
						_filter.set_protocol('SWITCH_OPERATION')
						_filter.set_language('BUS')
						_filter.set_ontology('virtual')
						waiter = shared.HandleMessage(self.agent, opening_request, _filter)
						self.agent.add_behaviour(waiter)
						self.wait_return(waiter)
				elif response.get_performative() == ACLMessage.REJECT_PROPOSAL:
					# DEBUG
					display(self.agent, '%s %s the conection.' % (color.blue(response.get_sender().getName()), color.red('REJECTED')))
			else:
				# DEBUG
				display(self.agent, 'Stoping to send the recovery proposals due to an alteration in the network.')
				break
		# > Sets the recovery state for this requisition as 'False'
		functions.send_recovery_state(self.agent, False)
		# > Removing this proposal from analisys (if this proposal is still
		# being analyzed)
		with self.agent.current_proposal.lock:
			if self.agent.current_proposal.data == self.analyzed_proposal:
				self.agent.current_proposal.data = None
				# DEBUG
				#display(self.agent, color.magenta('CLEANING the analyzed proposal field.', 'd'))
			# DEBUG
			else:
				# DEBUG
				display(self.agent, color.magenta('NOT CLEANING the analyzed proposal field because this proposal is outdated.', 'd'))
		self._done = True
	

	def done(self):
		return self._done