'''
Common functions module
-----------------------

This module meants to get together the functions that are recurrent while
developing the behaviours of the agents of Smash-py. All the functions placed
here should describe exactly for which agent they are meant.

Author: @italocampos
'''

from pade.acl.messages import ACLMessage
import pickle


def convert_into_aid(agent, line_indexes):
	''' Converts a list of indexes of lines into a list of aid of lines.

	This function is meant to BusAgent.
	
	Parameters
	----------
	agent : BusAgent
		The BusAgent that executes this function.
	line_indexes : list
		The list of idexes of the lines to be converted into AIDs.
	
	Returns
	-------
	list
		The list with the corresponding AID objects of the line indexes.
	'''

	aids = list()
	for index in line_indexes:
		for neighbor in agent.neighbors:
			if index == neighbor['line_id']:
				aids.append(neighbor['line_aid'])
				break
	return aids



def get_neighbor_of_line(agent, line_aid):
	''' Returs the neighbor placed in the other side of the line. 
	
	Used to get the neighbor on the other side of line. This function is meant
	to be used by BusAgent.

	Parameters
	----------
	agent : BusAgent
		The BusAgent that executes this function.
	line_aid : AID
		The AID of the line which is wanted to know the neighbor.

	Returns
	-------
	dict
		The dict data of the neighbor placed in the other side of line.
	'''

	for neighbor in agent.neighbors:
		if line_aid == neighbor['line_aid']:
			return neighbor



def get_line_to_neighbor(agent, neighbor_aid):
	''' Returs the line between the requester agent and the `neighbor_aid`.
	
	Used to get the line between two BusAgents. This function is meant to be
	used by BusAgent.

	Parameters
	----------
	agent : BusAgent
		The BusAgent that executes this function.
	neighbor_aid : AID
		The AID of the neighbor which is wanted to know the line.

	Returns
	-------
	AID
		The AID of the line between the requester and the 'neighbor_aid'.
	'''

	for neighbor in agent.neighbors:
		if neighbor_aid == neighbor['bus_aid']:
			return neighbor['line_aid']



def max_degree(degrees):
	''' Returns the element with the biggest degree fom the list.
	
	Gets a list of dicts and returns the element with biggest degree. This
	element is removed from the list. The list has dicts as elements in the
	following form:
	{'bus_aid': <bus aid>,
	'degree': <degree of the bus>}

	Parameters
	----------
	degrees : list
		The list with the neighbors and their degrees.
	
	Returns
	-------
	dict
		The element of the list with the biggest degree. Return None if the
		provided list is empty.
	'''

	if degrees != []:
		max_degree = 0
		for i in range(1, len(degrees)):
			if degrees[i]['degree'] > degrees[0]['degree']:
				max_degree = i
		return degrees.pop(max_degree)



def send_recovery_state(agent, recovery_state):
	''' Sends the provided recovery state to NetworkAgent.

	Use `True` to set the recovery state as 'active' and `False` to set it as
	'inactive'. This function is meant to be used by BusAgent.

	Parameters
	----------
	agent : BusAgent
		The agent that executes the function.
	recovery_state : bool
		The bool that indicate if the recovery state is active or inactive.
	'''

	notification = ACLMessage(ACLMessage.INFORM)
	notification.set_protocol('SET_RECOVERY_STATE')
	notification.set_language('BUS')
	notification.add_receiver(agent.NETWORK)
	notification.set_content(pickle.dumps({
		'id': agent.id,
		'state': recovery_state,
	}))
	agent.send(notification)



def confirm_topology_usage(agent):
	''' Sends a message to NetworkAgent confirming a net configuration.

	This function is meant to BusAgent.

	Parameters
	----------
	agent : pade.core.Agent
		The agent that sends the confirmation of the usage of the topology.

	'''

	confirmation = ACLMessage(ACLMessage.INFORM)
	confirmation.set_protocol('TOPOLOGY_USAGE')
	confirmation.set_language('BUS')
	confirmation.add_receiver(agent.NETWORK)
	confirmation.set_content('True')
	agent.send(confirmation)



def disconfirm_topology_usage(agent):
	''' Sends a message to NetworkAgent disconfirming a net configuration.

	This function is meant to BusAgent.

	Parameters
	----------
	agent : pade.core.Agent
		The agent that sends the disconfirmation of the usage of the topology.

	'''

	disconfirmation = ACLMessage(ACLMessage.INFORM)
	disconfirmation.set_protocol('TOPOLOGY_USAGE')
	disconfirmation.set_language('BUS')
	disconfirmation.add_receiver(agent.NETWORK)
	disconfirmation.set_content('False')
	agent.send(disconfirmation)