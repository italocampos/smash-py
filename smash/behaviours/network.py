import pickle, networkx, pandapower as pp, color, datetime
from pade.acl.filters import Filter
from pade.acl.messages import ACLMessage
from pade.behaviours.types import OneShotBehaviour, CyclicBehaviour, TickerBehaviour
from pade.misc.utility import display
from smash import tools, log



class SimulateEnergy(OneShotBehaviour):
	''' This behavior simulates the energy behavior in the system. This
	simulation is made over the real network.
	'''

	def action(self):
		# This lock prevents inconsistent data
		with self.agent.network_lock:
			# Creating a bus list with all states set in True
			states = [True for _ in self.agent.network.bus.values]
			# Looking for unsupplied buses and marking them as False
			for bus_index in list(pp.topology.unsupplied_buses(self.agent.network)):
				states[bus_index] = False
			# Passing the data to DataResource
			self.agent.network_data.set_buses_state(states)



class CheckCycle(OneShotBehaviour):
	''' This behavior cheks if the network topology has cycles. This
	verification is made over the real network.
	'''

	def action(self):
		with self.agent.network_lock:
			# Looking for cycles
			graph = pp.topology.create_nxgraph(self.agent.network)
			try:
				cycle = networkx.find_cycle(graph)
				display(self.agent, color.red('### A cycle was found in ', 'bold') + cycle)
			except(networkx.exception.NetworkXNoCycle):
				pass



class LineManager(CyclicBehaviour):
	''' This behavior receives messages from LineAgent informing the opening or
	closing of the lines of the network. This behaviour	also sends a message
	confirming the switch operation. '''

	def action(self):
		message = self.read()

		# Creating the main filter
		_filter = Filter()
		_filter.set_performative(ACLMessage.REQUEST)
		_filter.set_protocol('SWITCH_OPERATION')
		_filter.set_language('LINE')

		if _filter.filter(message):
			content = pickle.loads(message.get_content())
			if content['operation'] == 'OPEN':
				if content['net_type'] == 'real':
					with self.agent.network_lock:
						if not self.agent.is_switch_opened(content['switch_id']):
							self.agent.open_switch(content['switch_id'])
				elif content['net_type'] == 'virtual':
					with self.agent.virtual_network_lock:
						if not self.agent.is_virtual_switch_opened(content['switch_id']):
							self.agent.open_virtual_switch(content['switch_id'])
			elif content['operation'] == 'CLOSE':
				if content['net_type'] == 'real':
					with self.agent.network_lock:
						if not self.agent.is_switch_closed(content['switch_id']):
							self.agent.close_switch(content['switch_id'])
				elif content['net_type'] == 'virtual':
					with self.agent.virtual_network_lock:
						if not self.agent.is_virtual_switch_closed(content['switch_id']):
							self.agent.close_virtual_switch(content['switch_id'])

			# There is no stablished criteria to set a switch operation as
			# UNSUCCESSFUL. Replying as SUCCESS operation.
			reply = message.create_reply()
			reply.set_performative(ACLMessage.INFORM)
			reply.set_content('SUCCESS')
			self.send(reply)
			# Updating the network data
			self.agent.add_behaviour(SimulateEnergy(self.agent))
			self.agent.add_behaviour(CheckCycle(self.agent))



class LineFileMonitor(TickerBehaviour):
	''' This behavior monitors the line file and updates the global control
	line list. '''

	def on_tick(self):
		# Reads the lines json file
		self.agent.network_data.read_line_file()



class PowerFlowListener(CyclicBehaviour):
	''' This behaviour listens for power flow requisitions and reply with the
	required network data. '''

	def action(self):
		# > Waits for the PF calculation lock get free
		self.agent.wait_pf()

		# > Continues the execution
		message = self.read()

		_filter = Filter()
		_filter.set_performative(ACLMessage.REQUEST)
		_filter.set_protocol('POWER_FLOW')
		_filter.set_language('BUS')
		
		if _filter.filter(message):
			# > Locks the PF until the PF requester releases it
			self.agent.lock_pf()
			# DEBUG
			#display(self.agent, color.magenta('The agent %s locked the PF.' % message.get_sender().getName(), 'dim'))

			content = pickle.loads(message.get_content())
			# > A list to put the results according with the requisitions
			results = list()
			# > Checking if the PF needs to be taken over the virtual or real
			# network
			if message.get_ontology() == 'real':
				with self.agent.network_lock:
					for configuration in content:
						# >> Creating a copy of the real network and performing
						# the power flow
						results.append(self.perform_power_flow(self.agent.network.deepcopy(), configuration))
			elif message.get_ontology() == 'virtual':
				with self.agent.virtual_network_lock:
					for configuration in content:
						# >> Creating a copy of the virtual network and
						# performing the power flow
						results.append(self.perform_power_flow(self.agent.virtual_network.deepcopy(), configuration))

			# Creating the response and sending the reply.
			reply = message.create_reply()
			reply.set_performative(ACLMessage.INFORM)
			reply.set_content(pickle.dumps(results))
			self.send(reply)
	

	def perform_power_flow(self, network, net_parameters):
		''' Performs the suitable power flow.
		
		Performs the power flow in the provided `network` using the
		provided `lock`. This method returns a dict with the results of the
		power flow.

		Parameters
		----------
		network : pandapower.auxiliary.pandapowerNet
			The copy of the network to perform the power flow. This may be a
			real or a virtual network.
		net_parameters : dict
			A dict containing the parameters of the power flow. The dict must
			have the following format:
			{'lines_to_open' : list,
			'lines_to_close' : list,
			'reference_bus' : int}
		
		Returns
		-------
		dict
			A dict with the power flow results. The dic has the following form:
			{'result': bool,
			'voltage': float}
		'''

		# Performing the switch operations locally because that is
		# a virtual operation
		for line in net_parameters['lines_to_open']:
			network.switch.closed.at[line] = False
		for line in net_parameters['lines_to_close']:
			network.switch.closed.at[line] = True

		# Validating the switch operations
		if tools.validate_network(network):
			return {
				'result': True,
				'voltage': network.res_bus.vm_pu[net_parameters['reference_bus']]}
		else:
			# DEBUG
			#display(self.agent, 'This connection violates the network constraints. Reference bus: %d.' % content['reference_bus'])
			return {
				'result': False,
				'voltage': None}



class BusDataListener(CyclicBehaviour):
	''' This behaviour listens for buses requests about their network data,
	like voltage profile. '''

	def action(self):
		message = self.read()

		_filter = Filter()
		_filter.set_performative(ACLMessage.REQUEST)
		_filter.set_protocol('BUS_DATA')
		_filter.set_language('BUS')
		
		if _filter.filter(message):
			# Creating the reply to be handled by each type of request
			reply = message.create_reply()
			content = pickle.loads(message.get_content())
			if content['parameter'] == 'voltage':
				reply.set_performative(ACLMessage.INFORM)
				# > Checking if the PF needs to be taken over the virtual or real
				# network
				if message.get_ontology() == 'real':
					with self.agent.network_lock:
						pp.runpp(self.agent.network)
						reply.set_content(str(self.agent.network.res_bus.vm_pu.at[content['bus_id']]))
				elif message.get_ontology() == 'virtual':
					with self.agent.virtual_network_lock:
						pp.runpp(self.agent.virtual_network)
						reply.set_content(str(self.agent.virtual_network.res_bus.vm_pu.at[content['bus_id']]))
			elif content['parameter'] == 'supply':
				# > Checks if the requester bus is supplied in the virtual net
				reply.set_performative(ACLMessage.INFORM)
				with self.agent.network_lock:
					# >> Getting the unsupplied buses
					unsupplied_buses = list(pp.topology.unsupplied_buses(self.agent.virtual_network))
					reply.set_content(str(content['bus_id'] not in unsupplied_buses))
			else:
				reply.set_performative(ACLMessage.NOT_UNDERSTOOD)
				reply.set_content('UNKNOWN OPTION')
			# Sending the suitable response
			self.send(reply)



class RecoveryStateListener(CyclicBehaviour):
	''' This behaviour meants to receive the messages from BusAgent about their
	recovery states.
	'''

	def action(self):
		message = self.read()
		_filter = Filter()
		_filter.set_performative(ACLMessage.INFORM)
		_filter.set_protocol('SET_RECOVERY_STATE')
		_filter.set_language('BUS')
		if _filter.filter(message):
			content = pickle.loads(message.get_content())
			self.agent.set_recovery_state(content['id'], content['state'])
			# DEBUG
			#display(self.agent, color.magenta('The agent %s set its recovery state as %s.' % (message.get_sender().getName(), content['state']), 'd'))
			# DEBUG
			#display(self.agent, color.magenta('Recovery state of the buses: %s' % self.agent.recovery_state_of_the_buses, 'd'))
			# > If the network got out from a 'recovering' state, then the
			# virtual network overrides the real network.
			if not self.agent.system_recovery_state():
				# Calculates the elapsed time in the recovery process
				time_delta = datetime.datetime.now() - self.agent.time
				# Resets the initial time
				#self.agent.time = None
				# DEBUG
				#display(self.agent, color.magenta('The final recovery state of the buses are: %s' % self.agent.recovery_state_of_the_buses, 'd'))
				with self.agent.network_lock:
					with self.agent.virtual_network_lock:
						self.agent.network = self.agent.virtual_network.deepcopy()
				# DEBUG
				display(self.agent, 'Making the switching operations in the real network.')
				# Simulate energy and check cycles
				self.agent.add_behaviour(SimulateEnergy(self.agent))
				self.agent.add_behaviour(CheckCycle(self.agent))
				# DEBUG
				display(self.agent, 'The recovery is ' + color.green('finished!', 'bold') + ' The final switches configuration is:')
				# DEBUG
				print(self.agent.network.switch)
				# DEBUG
				display(self.agent, color.yellow('The unsupplied buses are: %s' % list(pp.topology.unsupplied_buses(self.agent.network))))
				# DEBUG
				display(self.agent, 'Elapsed time: %s' % color.yellow(time_delta))
				# Making and writing the list with the state of the switches
				#display(self.agent, 'Writing the results in an output file.')
				switches = [int(state) for state in self.agent.network.switch.closed]
				log.write_log('topologies', str(switches) + ',', time_info = False)
				log.write_log('times', str(time_delta) + ',', time_info = False)



class TimeReceiver(CyclicBehaviour):
	''' This behaviour receives the messages with the time where the started
	the current recovery process. These messages are sent by LineAgents when
	detect a fault in a line.
	'''

	def action(self):
		message = self.read()
		_filter = Filter()
		_filter.set_performative(ACLMessage.INFORM)
		_filter.set_language('LINE')
		_filter.set_protocol('REGISTER_TIME')
		if _filter.filter(message):
			self.agent.time = pickle.loads(message.get_content())
			# DEBUG
			#display(self.agent, color.magenta('The initial time of recovery process was registered.', 'dim'))



class TopologyUsageListener(CyclicBehaviour):
	''' This behaviour receives messages of confirmation and disconfirmation
	about the usage of the tested topologies by the BusAgent.
	'''

	def action(self):
		message = self.read()
		_filter = Filter()
		_filter.set_performative(ACLMessage.INFORM)
		_filter.set_language('BUS')
		_filter.set_protocol('TOPOLOGY_USAGE')
		if _filter.filter(message):
			# DEBUG
			#display(self.agent, color.magenta('The agent %s released the PF with a %s.' % (message.get_sender().getName(), message.get_content()), 'dim'))
			self.agent.unlock_pf()