# Use this file to configure the Smash-py parameters

''' The network models available with this version of Smash-py are:
- 10-bus: A model created with Smash-py;
- 16-bus: Available in Patel A., Patel C. (2016);
- 33-bus: Available in Baran, Wu (1989);
- 119-bus: Available in Zhang D. et al. (2007);
'''

from pathlib import Path


# Put the home path of the system here
HOME_SYS = str(Path.home()) + '/workspaces/smash-py'

# Where the agents write their logs by default
LOG_PATH = HOME_SYS + '/data/logs/'

# The files that control the state of the lines
LINE_PATH = HOME_SYS + '/data/line.json'

# The full name of the NetworkAgent agent.
NETWORK = 'network@localhost:50001'

# The network model used in the simulation
NETWORK_MODEL = '16-bus'