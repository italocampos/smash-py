''' This Python file holds the global lists that controls
the states of lines and buses of the system.
'''

from smash import config
import threading, time, json


''' This class implements the operations to handle the
system control lists.
'''
class DataResource():

	def __init__(self, network):
		# Creating the external line json file
		dictionary = dict()
		for index in range(len(network.line.values)):
			dictionary.update({index: True})
		with open(config.LINE_PATH, 'w') as file:
			json.dump(dictionary, file)
		
		# Creating the initial lists
		self.BUS = [True for _ in network.bus.values]
		self.LINE = [True for _ in network.line.values]

		# Creating a lock object to acess the local lists
		self.bus_list_lock = threading.Lock()
		self.line_list_lock = threading.Lock()


	def set_buses_state(self, values_list):
		self.handle_bus_list(values_list, 'w')


	def get_bus_state(self, id):
		return self.handle_bus_list([], 'r')[id]


	def handle_bus_list(self, values_list, operation_type):
		with self.bus_list_lock:
			if operation_type == 'w':
				self.BUS = values_list
			elif operation_type == 'r':
				return self.BUS


	def set_lines_states(self, values_list):
		self.handle_line_list(values_list, 'w')


	def get_line_state(self, id):
		return self.handle_line_list([], 'r')[id]


	def handle_line_list(self, values_list, operation_type):
		with self.line_list_lock:
			if operation_type == 'w':
				self.LINE = values_list
			elif operation_type == 'r':
				return self.LINE
	

	def read_line_file(self):
		try:
			with open(config.LINE_PATH, 'r') as file:
				dictionary = json.loads(file.read())
			self.set_lines_states(list(dictionary.values()))
		except(json.JSONDecodeError):
			print('### Error reading the line control file. Trying again.')