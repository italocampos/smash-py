# Smash-py

##### The Smash for PADE

> Attention: This is a fork from Kelly Socorro's project, LAAI research member. 
Refer to the original repository to see the original project.


## About

This repository stores files for the project 'SMASH for PADE', which aims to 
provide a Python 3.x version for the SMASH system, by LAAI research group. 
SMASH is a acronym for 'Sistema Multiagente para Self-Healing' (Multiagent 
System for Self-Healing), which is a system to implements a distributed 
algorithm to power restore in Smart Grids.

This system is developed and maintained by the LAAI research group, by the 
master's research of Italo Campos.


## How Smash-py works?

This MAS simulates 4 (four) predefined network test models and uses just the
coordination between the agents to perform the recovery of the network after
detected a fault.

There's 3 (three) types of agents in the MAS:

- `BusAgent`: The agent that lives in the buses of the system. This agent is
an abstraction of the buses and the loads atached to this. This agent performs
the main tasks of message exchanging, decision making and others. These agents
take decisions about the states that the abstract network can assume.

- `LineAgent`: The agent that lives in the lines of the system. This agent
holds informations about the lines and control each switch of the models (both
sectionalizing and tie switches). These agents perform the switching
operations in the abstract net model.

- `NetworkAgent`: This agent works over the lowest layer of the network. It
performs the power flow calculation and makes effectivelly the switching
operations in the lowest level of the network model.

To know how the agents coordination is implemented, refer to my master's thesis
available here.


## Network models supported

The network models supported in this version of Smash-py are:

- 10-bus system: A test model developed with Smash-py to this research;
    + Voltage profile of the system: 11 kV
    + 10 buses (10 `BusAgent`)
    + 11 lines [8 sectionalizing switches, 3 tie switches] (11 `SwitchAgent`)
    + 1 `NetworkAgent` 


- 16-bus system: A test model used in the literature and described in Patel A.,
Patel C. (2016);
    + Voltage profile of the system: 11 kV
    + 16 buses (16 `BusAgent`)
    + 16 lines [13 sectionalizing switches, 3 tie switches] (16 `SwitchAgent`)
    + 1 `NetworkAgent`

- 33-bus system: A test model used in the literature and described in Baran, Wu
(1989);
    + Voltage profile of the system: 12.66 kV
    + 33 buses (33 `BusAgent`)
    + 37 lines [32 sectionalizing switches, 5 tie switches] (33 `SwitchAgent`)
    + 1 `NetworkAgent`

- 119-bus system: A test model used in the literature and described in ZHANG,
D.; FU, Z.; ZHANG, L. (2007);
    + Voltage profile of the system: 11 kV
    + 118 buses (118 `BusAgent`)
    + 132 lines [117 sectionalizing switch, 15 tie switches] (132 `SwitchAgent`)
    + 1 `NetworkAgent`


## Running Smash-py

To run the examples, first you need to install its dependencies. They are
listed below:

- [Pandapower](http://www.pandapower.org/);
- [Numba (optional)](https://numba.pydata.org/numba-doc/dev/user/installing.html);
- [PADE](https://github.com/italocampos/pade);
- [Color lib](https://github.com/italocampos/color);
- [Colorama](https://pypi.org/project/colorama/).

Excepting the Color lib, all dependencies can be downloaded with `pip`. Note
that the you must to use the PADE version in the link above.

After installed all dependencies, go to 'smash-py' folder and run
`pade start-runtime main.py`.

You can switch between different test modes in the config file
`smash-py/smash/config.py`. Here you can also set the path to the logs and the
control file of the lines.


## References

**PATEL, A. G.; PATEL, C.** Distribution network reconfiguration for loss
reduction. In: 2016 International Conference on Electrical, Electronics, and
Optimization Techniques (ICEEOT). IEEE, 2016. p. 3937–3941.
ISBN 978-1-4673-9939-5. Disponível em: <http://ieeexplore.ieee.org/document/7755453/>.

**ZHANG, D.; FU, Z.; ZHANG, L.** An improved TS algorithm for loss-minimum
reconfiguration in large-scale distribution systems. Electric Power Systems
Research, v. 77, n. 5-6, p. 685–694, apr 2007. ISSN 03787796. Disponível em:
<https://linkinghub.elsevier.com/retrieve/pii/S0378779606001477>.

**BARAN, M. E.; WU, F. F.** Network reconfiguration in distribution systems for
loss reduction and load balancing. IEEE Transaction on Power Delivery, v. 4, n.
2, p. 1401–1407, 1989.