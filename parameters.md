# Parameters for simulations

This file resumes the parameters and other specifications used in the
simulations on the mastering work of @italocampos.


## Network parameters

The parameters used to specify the constraints of _voltage variation_ and
_current variation_ in each test model.

| MODEL        | VOLTAGE VARIATION | CURRENT VARIATION |
| ------------ | ----------------- | ----------------- |
| **10-bus**   | 0.05              | 0.0               |
| **16-bus**   | 0.05              | 0.0               |
| **33-bus**   | 0.18              | 0.0               |
| **119-bus**  | 0.15              | 0.0               |


## Fault points

The selected lines to fail in each simulation. This parameter is splited in
many groups, as seen below.


### Simulations with a single fail

| SCENARIO       | 10-BUS | 16-BUS | 33-BUS | 119-BUS |
| -------------- | ------ | ------ | ------ | ------- |
| **Scenario 1** | [3]    | [4]    | [24]   | [28]    |
| **Scenario 2** | [0]    | [0]    | [8]    | [87]    |
| **Scenario 3** | [1]    | [9]    | [17]   | [63]    |
| **Scenario 4** | [9]    | [2]    | [5]    | [99]    |
| **Scenario 5** | -      | [11]   | [2]    | [62]    |


### Simulations with multiple fails

| SCENARIO       | 10-BUS | 16-BUS      | 33-BUS         | 119-BUS                |
| -------------- | ------ | ----------- | -------------- | ---------------------- |
| **Scenario 1** | [0, 6] | [4, 11]     | [6, 9, 12]     | [27, 28, 29]           |
| **Scenario 2** | [2, 3] | [0, 4]      | [2, 4, 21]     | [27, 99]               |
| **Scenario 3** | [0, 9] | [4, 7]      | [9, 17]        | [27, 62]               |
| **Scenario 4** | [1, 5] | [9, 10, 11] | [4, 6, 12, 25] | [66, 72, 89]           |
| **Scenario 5** | [1, 7] | [0, 3, 6]   | [22, 25]       | [67, 77, 99, 105, 113] |


### Simulations with consecutive recovery

| STEP       | 10-BUS | 16-BUS  | 33-BUS  | 119-BUS  |
| ---------- | ------ | ------- | ------- | -------- |
| **Step 1** | [0]    | [4]     | [24]    | [54]     |
| **Step 2** | [9]    | [7]     | [11]    | [30]     |
| **Step 3** | -      | [11]    | [15, 5] | [47, 67] |

This group of simulations is performed only with Smash-py, and is meant to show
the ability of the MAS to recovery the system without remap its topology.


# Data collected

This section describes the data collected in each simulation. For simulations
with the Tabu Search, some other data are collected in addition. These
meta-data are described below.

- **Simulation ID**: A identifier for each simulation;
- **Number of affected buses**: The number of buses affected by the faulted
line(s);
- **Number of recovered buses**: The number of buses recovered after run the
recovery algorithm;
- **Value of solution**: The value of the solution in the objective function
adopted in this research;
- **Time (seconds)**: The execution time of the recovery algorithm;
- **Opened swithces**: The switches that the algorithm opened during its
execution;
- **Closed swithces**: The switches that the algorithm closed during its
execution;
- **Fault lines**: The lines that failed in the simulation;


To the Tabu Search, in addition, the data below were collected:

- **Average**: The average of the values of the values of the objective
funtion in each one of the 10 executions of TS;
- **Standard deviation**: The standard deviation of the values of the objective
funtion in each one of the 10 executions of TS;

> To TS, the data of the previous section are based on the best solution found.